<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends MY_Controller {

	function __construct() {
	   parent::__construct();
       $this->load->library('common');
   }

	public function index(){
		// echo"<pre>";print_r($_POST);die;
		// die("Hi");
		/*start:test library*/
		   $resp=$this->common->UniqueKey();
		   echo"<pre>";print_r($resp);die;
		/*end*/
		$query=$this->db->query('SELECT * FROM users');
		echo "<pre>";print_r($query->result_array());die;
	}

	public function register(){
		// echo"<pre>";print_r($_POST);die;
		$response=array();
		if(!empty($_POST)){
			$error=$this->validateRegister($_POST);
			if($error['error']){
				$response=$error;
			}else{
				$data=$_POST;
				$data['password']=md5($_POST['password']);
				$data['created']=date("Y-m-d H:i:s");
				$data['token']=$this->common->UniqueKey();
				$data['name']=trim($data['name']);
				$data['mobile']=$data['country_code'].$data['phone'];
				$sql = "INSERT INTO users (role,name,email,device_id,term_cond,password,token,device_type,phone,created_date) VALUES(?,?,?,?,?,?,?,?,?,?)";
                $this->db->query($sql, array($data['user_role'], $data['name'], $data['email'], $data['device_id'],$data['term_cond'],$data['password'],$data['token'],$data['device_type'],$data['mobile'],$data['created']));
				if($this->db->affected_rows()>0){
					$data['id']=$this->db->insert_id();
					$response['error']=false;
                                unset($data['password']);unset($data['confirm_password']);
           			$response['statuscode']=200;
           			$response['msg']="User has been registered successfully.";
           			$response['user']=$data;
           			 /*---Sending Email-------Start---------------*/
                    $subject = 'User Registered ArrivalAPP';
       

                   $registerdata = array(
                             'data'=> $data
                             );
          
                   $message = $this->load->view('emails/register.php',$registerdata,TRUE);

                   $this->send_Email(Email_From,$data['email'],$subject,$message);
                  
                   /*---Sending Email--------End-----------------*/
				}else{
					$response['error']=true;
           			$response['statuscode']=513;
           			$response['msg']="Server error";
				}
			}
		}else{
           $response['error']=true;
           $response['statuscode']=512;
           $response['msg']="Invalid Request";
		}
		echo json_encode($response);die;
	}

	public function validateRegister($data=null){
		// echo"<pre>";print_r(strlen($data['phone']));die;
		$this->load->model('User');
		$error=array('error'=>false);
		$reg='/^[A-Za-z0-9 _]+$/';
		$regex = "/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i"; 
		$regphone = "/^[1-9][0-9]*/";
		$regphone1='/^[0-9]+$/';
		$stronpass='/^(?=.*[!@#$%]{1,})/' ;
		$stronpass1='/^(?=.*[A-Za-z]{1,})/' ;
		$stronpass2='/^(?=.*[0-9]{1,})/' ;
		if(!empty($data)){
			if (!isset($data['name']) || strlen(trim($data['name'])) <= 0){
				$error['error']=true;
				$error['statuscode']=303;
				$error['msg']="Please enter the name.";
			}else if (!isset($data['email']) || strlen(trim($data['email'])) <= 0) {
				$error['error']=true;
				$error['statuscode']=302;
				$error['msg']="Please enter the email.";
			}else if(!isset($data['password']) || strlen(trim($data['password'])) <= 0){
				$error['error']=true;
				$error['statuscode']=304;
				$error['msg']="Please enter the password.";
			}else if(!isset($data['user_role']) || strlen(trim($data['user_role'])) <= 0){
				$error['error']=true;
				$error['statuscode']=305;
				$error['msg']="user_role is required.";
			}else if(!isset($data['device_type']) || strlen(trim($data['device_type'])) <= 0){
				$error['error']=true;
				$error['statuscode']=306;
				$error['msg']="device_type is required.";
			}else if(!isset($data['phone']) || strlen(trim($data['phone'])) <= 0){
				$error['error']=true;
				$error['statuscode']=402;
				$error['msg']="Please enter the phone number.";
			}else if(!isset($data['country_code']) || strlen(trim($data['country_code'])) <= 0){
				$error['error']=true;
				$error['statuscode']=407;
				$error['msg']="country code is required in phone.";
			}else if(!isset($data['device_id']) || strlen(trim($data['device_id'])) <= 0){
				$error['error']=true;
				$error['statuscode']=307;
				$error['msg']="device_id is required.";
			}else if(!isset($data['term_cond']) || strlen(trim($data['term_cond'])) <= 0){
				$error['error']=true;
				$error['statuscode']=308;
				$error['msg']="Please accept the term and conditions.";
			}



			/*else if ($this->common->checkfield('email',$data['email'])){
					$error['error']=true;
				    $error['statuscode']=401;
				    $error['msg']="Email is already registered.";
			}else if ($this->common->checkfield('phone',$data['country_code'].$data['phone'])){
					$error['error']=true;
				    $error['statuscode']=403;
				    $error['msg']="Phone number is already registered.";
			}*/

			 else if($this->User->isUserExistEmail($data['email'],$data['user_role'])){

                    $error['error']=true;
				    $error['statuscode']=401;
				    $error['msg']="Email is already registered.";
		     }


		     else if($this->User->isUserExistPhone($data['user_role'],$data['country_code'].$data['phone'])){

                    $error['error']=true;
				    $error['statuscode']=403;
				    $error['msg']="Phone number is already registered.";
		     }






			else if (strlen($data['name'])<2 || strlen($data['name'])>20){
					$error['error']=true;
				    $error['statuscode']=404;
				    $error['msg']="Name should be within 2-20 characters.";
			}else if (!preg_match($reg,$data['name'])){
					$error['error']=true;
				    $error['statuscode']=405;
				    $error['msg']="Please enter valid name.";
			}else if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)){
              		$error['error']=true;
				    $error['statuscode']=406;
				    $error['msg']="Please enter valid email.";
            }else if (!preg_match($regex, $data['email'])){
              		$error['error']=true;
				    $error['statuscode']=406;
				    $error['msg']="Please enter valid email.";
            }else if (strlen($data['phone'])!=10){
              		$error['error']=true;
				    $error['statuscode']=408;
				    $error['msg']="Phone number should be 10 characters.";
            }else if (!preg_match($regphone, $data['phone'])){
              		$error['error']=true;
				    $error['statuscode']=409;
				    $error['msg']="Please enter valid phone number.";
            }else if (!preg_match($regphone1, $data['phone'])){
              		$error['error']=true;
				    $error['statuscode']=409;
				    $error['msg']="Please enter valid phone number.";
            }else if (strlen($data['password'])<6 || strlen($data['password'])>12){
              		$error['error']=true;
				    $error['statuscode']=410;
				    $error['msg']="Password should be 6-12 characters.";
            }else if (!preg_match($stronpass, $data['password'])){
              		$error['error']=true;
				    $error['statuscode']=411;
				    $error['msg']="Password should contain atleast one special characters.";
            }else if (!preg_match($stronpass1, $data['password'])){
              		$error['error']=true;
				    $error['statuscode']=412;
				    $error['msg']="Password should contain atleast one numeric characters.";
            }else if (!preg_match($stronpass2, $data['password'])){
              		$error['error']=true;
				    $error['statuscode']=412;
				    $error['msg']="Password should contain atleast one alphabetical characters.";
            }else if (!in_array($data['user_role'], array('admin','user'))){
              		$error['error']=true;
				    $error['statuscode']=413;
				    $error['msg']="User type is not valid.";
            }else if (!in_array($data['device_type'], array('android','ios'))){
              		$error['error']=true;
				    $error['statuscode']=414;
				    $error['msg']="Device type is not valid.";
            }else if ($data['term_cond']!=1){
              		$error['error']=true;
				    $error['statuscode']=415;
				    $error['msg']="Please accept term and condition.";
            }else if(!isset($data['confirm_password']) || strlen(trim($data['confirm_password'])) <= 0){
				$error['error']=true;
				$error['statuscode']=416;
				$error['msg']="Confirm Password is required.";
			}else if($data['password']!=$data['confirm_password']){
				$error['error']=true;
				$error['statuscode']=417;
				$error['msg']="Password and Confirm Password does not matched.";
			}else{
				$error['error']=false;
			}
		}else{
			$error['error']=true;
			$error['statuscode']=418;
			$error['msg']="Post data is not valid.";
		}
		return $error;
	}
	// public function login(){

 //   $response = array();
 //   $error = array(); 
	//  if (
	//  	$this->input->server('REQUEST_METHOD') == 'POST' 
	//  	&& 
	//  	( 
	//  		 (
	//  		 	isset($_POST['email']
	//  		 		)
	//  		 	)
	//  		 && (
	//  		 	isset($_POST['password']
	//  		 		)
	//  		 	) 
	//  		)
	//  	)
	//  {
 //        $error = $this->validate_login($_POST);
 //        $response = $error;
 //    }else
	// {
 //         $response['message'] = 'Invalid Request';
 //         $response['statuscode'] = 419;	
	// }
 //          $this->output
 //             ->set_content_type('application/json')
 //             ->set_output(json_encode($response));
 //    }

 //    public function validate_login($login_data){  
    	 
 //    	       $this->load->model('User');
 //    	       $error = array();
 //    	       if(empty($login_data['email'])){
 //    	       	$error['message'] = 'Please enter the email.';
 //    	       	$error['statuscode'] = 	302;
 //    	       }else if(empty($login_data['password'])){
 //    	       	$error['message'] = 'Please enter the password.';
 //    	        $error['statuscode'] = 	304;
 //    	       }
 //    	       /*if(!$this->common->validate_email($login_data['email'])){
 //                $error['message'] = 'Email is not valid';
 //    	       	$error['statuscode'] = 	301;
 //    	       }
 //    	       $error_pass = $this->common->validate_password($login_data['password']);
    	        
    	        
 //    	        if($error_pass['statuscode']!=200){  
                   
 //                   $error = $error_pass;
 //    	        }*/
 //    	        else
 //    	        {
	//     	        $error_DB = $this->User->getUserByEmail_Password($login_data['email'],$login_data['password']);
	//     	        if($error_DB['statuscode']!=200){
                               
	//     	        	$error = $error_DB;
	//     	        }else{
 //                                unset($error_DB['data']->password);
	//     	        	$error = $error_DB;
	//     	        }
 //              }
    	        

 //     return $error;

 //    }

	public function login(){

   $response = array();
   $error = array(); 
	 if (
	 	$this->input->server('REQUEST_METHOD') == 'POST' 
	 	&& 
	 	( 
	 		 (
	 		 	isset($_POST['email']
	 		 		)
	 		 	)
	 		 && (
	 		 	isset($_POST['password']
	 		 		)
	 		 	)
	 		 	&& (
	 		 	isset($_POST['user_role']
	 		 		)
	 		 	) 
	 		)
	 	)
	 {
        $error = $this->validate_login($_POST);
        $response = $error;
    }else
	{
         $response['message'] = 'Invalid Request';
         $response['statuscode'] = 419;	
	}
          $this->output
             ->set_content_type('application/json')
             ->set_output(json_encode($response));
    }

    public function validate_login($login_data){  
    	 
    	       $this->load->model('User');
    	       $error = array();
    	       if(empty($login_data['email'])){
    	       	$error['message'] = 'Please enter the email.';
    	       	$error['statuscode'] = 	302;
    	       }else if(empty($login_data['password'])){
    	       	$error['message'] = 'Please enter the password.';
    	        $error['statuscode'] = 	304;
    	       }else if(empty($login_data['user_role'])){
    	       	$error['statuscode']=305;
				$error['message']="Please enter the user role.";
    	       }
    	       /*if(!$this->common->validate_email($login_data['email'])){
                $error['message'] = 'Email is not valid';
    	       	$error['statuscode'] = 	301;
    	       }
    	       $error_pass = $this->common->validate_password($login_data['password']);
    	        
    	        
    	        if($error_pass['statuscode']!=200){  
                   
                   $error = $error_pass;
    	        }*/
    	        else
    	        {
	    	        $error_DB = $this->User->getUserByEmail_Password($login_data['email'],$login_data['password'],$login_data['user_role']);
	    	        if($error_DB['statuscode']!=200){
                               
	    	        	$error = $error_DB;
	    	        }else{
                                unset($error_DB['data']->password);
	    	        	$error = $error_DB;
	    	        }
              }
    	        

     return $error;

    }

     public function logout(){

	   $response = array();
	   $error = array(); 
	 if (
	 	$this->input->server('REQUEST_METHOD') == 'POST' 
	 	&& 
	 	( 
	 		 (
	 		 	isset($_POST['user_id']
	 		 		)
	 		 	)
	 		 && (
	 		 	isset($_POST['token']
	 		 		)
	 		 	) 
	 		)
	 	)
	 {
        $error = $this->validate_logout($_POST);
        $response = $error;
    }else
	{
         $response['message'] = 'Invalid Request.';
         $response['statuscode'] = 501;	
	}
          $this->output
             ->set_content_type('application/json')
             ->set_output(json_encode($response)); 

    }

    public function validate_logout($logout_data){  
    	 
    	       $this->load->model('User');
    	       $error = array();
    	       if(empty($logout_data['user_id'])){
    	       	$error['message'] = 'User id is required';
    	       	$error['statuscode'] = 	313;
    	       }
    	      else
    	        {
	    	        $error_DB = $this->User->getUserById($logout_data['user_id'],$logout_data['token']);
	    	        if($error_DB['statuscode']!=200){
	    	        	$error = $error_DB;
	    	        }else{
	    	        	$error = $error_DB;
	    	        }
              }
    	        

     return $error;

    }

/**********Api for Term and Condition code start**********/
    public function term_cond()
    {

    if($this->input->server('REQUEST_METHOD')=='GET')
    {

	        $this->load->model('Page');
			$response = array();
			$error_DB = $this->Page->termCondition('term_cond');
			$response=$error_DB;
		}else{
			$response['message'] = 'Invalid Request.';
            $response['statuscode'] = 501;
		}
			 
			          $this->output
			             ->set_content_type('application/json')
			             ->set_output(json_encode($response));
	  
	  }  
      /**********Api for Term and Condition code end **********/

       /**********Api for about us code start**********/

     public function about()
    {
		   
     if($this->input->server('REQUEST_METHOD')=='GET')
     {
		   $this->load->model('Page');
			$response = array();
			$error_DB = $this->Page->about('about_us');
			$response=$error_DB;
	}else{
		$response['message'] = 'Invalid Request.';
         $response['statuscode'] = 501;

	}
			 
			          $this->output
			             ->set_content_type('application/json')
			             ->set_output(json_encode($response));

	  
	  }      
    /**********Api for about us code end **********/


  /*----------Api for get all network by userid-----------*/
	public function allNetwork()
	{
		$this->load->model('Network');
			$response=array();
       	    $error = array(); 
	 if (
	 	$this->input->server('REQUEST_METHOD') == 'GET' 
	 	&& 
	 	( 
	 		 (
	 		 	isset($_GET['user_id']
	 		 		)
	 		 	)
	 		 && (
	 		 	isset($_GET['token']
	 		 		)
	 		 	) 
	 		)
	 		  		
	 	)
	 {
        if(empty($_GET['user_id'])){
    	       	$response['message'] = 'User id is required.';
    	       	$response['statuscode'] =313;
    	       }
    	       else if (empty($_GET['token'])) {
    	     	$response['message'] = 'Token is required.';
    	       	$response['statuscode'] =313;
    	       }
    	    else{
			$data=$this->Network->getNetworks($_GET['user_id'],$_GET['token']);
			if($data['statuscode']!=221){
	    	     $response = $data;
	    	     }else{
	    	     $response=$data;						
	    	      }			
				}
            }else
	{
         $response['message'] = 'Invalid Request.';
         $response['statuscode'] = 501;	
	}
			
			$this->output->set_content_type('application/json')
						->set_output(json_encode($response));

	}




    /*----------------End code---------------*/

		/*------- Api for getfeedback----------*/
		public function getFeedback()
		{
			$response=array();
			$error=array();
			if($this->input->server('REQUEST_METHOD')=='GET'
			&&(
				 (isset($_GET['user_id']))

				)
				)
			{
				$error=$this->validate_getfeedback($_GET);
			    $response=$error;
			}
			else
				{
					 $response['message'] = 'Invalid Request.';
			         $response['statuscode'] = 501;	
				}

				 $this->output
			             ->set_content_type('application/json')
			             ->set_output(json_encode($response)); 
		}

		public function validate_getfeedback($feedback_data)
		{

			$this->load->model('Feedback');
			$error=array();
			if(empty($feedback_data['user_id']))
			{
					$error['message'] = 'User id is required.';
		    	    $error['statuscode'] = 	313;
			}
			else if(!is_numeric($feedback_data['user_id']))
			{
				$error['message'] = 'User id should be numeric.';
		    	$error['statuscode'] = 	313;

			}
			
			else{
				$error_DB=$this->Feedback->getFeedback($feedback_data['user_id'],$feedback_data['token']);
				if($error_DB['statuscode'] !=205)
				{			
				$error = $error_DB;
			     }
			     else
			     {
			     $error = $error_DB;
			      }
			}
			return $error;

		}

/*------- Api for getfeedback code end----------*/

/*------- Api for savefeedback----------*/
public function saveFeedback()
{
	$response=array();
	$error=array();
	if($this->input->server('REQUEST_METHOD')=='POST'
		&&(
		     (isset($_POST['user_id'])) && (isset($_POST['feedback']))
		  )
		)
	{
		
		$error=$this->validate_feedback($_POST);
		$this->emailToArrivalAdmin($_POST['user_id'],$_POST['feedback']);
		$response=$error;
	}
	else
	{
		 $response['message'] = 'Invalid Request.';
         $response['statuscode'] = 501;	
	}
	 $this->output
             ->set_content_type('application/json')
             ->set_output(json_encode($response)); 

}
public function emailToArrivalAdmin($user_id,$feedback){

      $this->load->model('User');
      $user_data = $this->User->getUser($user_id);
      
      /*---Sending Email to APP owner Start---------------*/
          $subject = 'New Feedback';
          $message = $user_data[0]->name.' '.'Given the new feedback:'.' '.$feedback;
       //$this->send_Email(Email_From,$error['data']->email,$subject,$message);

       //$message = $this->load->view('emails/forgotpassword.php',$newPass,TRUE);

       $this->send_Email($user_data[0]->email,Admin_Email_To,$subject,$message);

       return; 
       /*---Sending Email to App owner End-----------------*/


}
public function validate_feedback($feedback_data)
{

	$this->load->model('Feedback');
	$error=array();
	if(empty($feedback_data['user_id']))
	{
			$error['message'] = 'User id is required.';
    	             $error['statuscode'] = 313;
	}
       else if(!is_numeric($feedback_data['user_id']))
			{
			$error['message'] = 'User id should be numeric.';
		    	$error['statuscode'] =313;

			}
	else if (empty($feedback_data['feedback'])) {
		
			$error['message'] = 'Feedback is required.';
    	         $error['statuscode'] = 313;
	}
	else{
		$error_DB=$this->Feedback->addFeedback($feedback_data['user_id']);
		if($error_DB['statuscode'] !=204)
		{			
		$error = $error_DB;
	     }
	     else
	     {
	     $error = $error_DB;
	      }
	}
	return $error;

}
/*------- Api for feedback code end----------*/

      
    /*---Forgot Password API- START   ----------*/


  /*

   Method:- POST

   Request parameter:- {email}
   Send password in email.
   Response Params:-{   error:false,statuscode:202,msg:”email has been sent successfully.” }
   Validation error messages with status code:-
   statuscode:”301”, msg:”email is not valid.”,
   statuscode:”302”, msg:”email is required.”,
   statuscode:”310”, msg:”email does not exist.”,

   function send_Email($from,$to,$subject=null,$message=null){
 
  */

   public function forgotPassword(){  

     $response = array();
     $this->load->model('User');
     $error = array();
     if(
     	  $this->input->server('REQUEST_METHOD')=='POST'
     	  &&
     	  isset($_POST['email']) && !empty($_POST['email'])
     	  &&
     	  isset($_POST['user_role']) && !empty($_POST['user_role'])
     	){
             
     	  $error = $this->User->getUserByEmail($_POST['email'],$_POST['user_role']);

     }else{
	 if (!isset($_POST['email']) || strlen(trim($_POST['email'])) <= 0) {
	    $response['message'] = 'Please enter the Email.';
     	    $response['statuscode'] = 302;

	}else if (!isset($_POST['user_role']) || strlen(trim($_POST['user_role'])) <= 0) {
	       $response['message'] = 'Please enter the User role.';
     	    $response['statuscode'] = 305;

	}
	else{
            $response['message'] = 'Email does not exist.';
     	    $response['statuscode'] = 501;
	}
     	echo json_encode($response);die;
     }
      if($error['statuscode']==200){
       $getPassword = $this->User->update_and_getPassword($error['data']->id);

       $subject = 'Password updated';
       

       $newPass = array(
             'Newpassword'=> $getPassword
                 );
       
        $message = $this->load->view('emails/forgotpassword.php',$newPass,TRUE);

       $this->send_Email(Email_From,$error['data']->email,$subject,$message);
       


       $response['statuscode']=202;
       $response['error'] = 'false';
       $response['message']='Password has been sent to your email successfully';
       

      }else{
      	$response = $error;
      }
       $this->output
             ->set_content_type('application/json')
             ->set_output(json_encode($response));  
        
     }

    /*---Forgot Password API- END   ----------*/
     /*----Start Add Network Api 11-January-2016 Evening Start---------*/

    /*

    Add Network api:
    Method:- POST

    Request param: {user_id,name,ssid,network_type,router_type,public_ip,netw_key,conf_key,token,router_id}
   Response Params:-{   error:false,statuscode:210,msg:”network has been added successfully.”}
           Validation error messages with status code:-
           statuscode:”313”, msg:”User id is required.”,
           statuscode:”317”, msg:”network name is required.”,
           statuscode:”318”, msg:”ssid  is required.”,
           statuscode:”319”, msg:”router  is required.”,
           statuscode:”320”, msg:”public ip  is required.”,
           statuscode:”321”, msg:”network key  is required.”,
           statuscode:”322”, msg:”network key and confirmed does not match.”,


            statuscode:”350”, msg:”Confirm Key is required”,
            statuscode:”351”, msg:”Token can not be empty”,
            statuscode:”352”, msg:”Router Id is required”,
            statuscode:”353”, msg:”Please enter valid Ip Address”,
            statuscode:”354”, msg:”Network Type is required”,
            statuscode:”357”, msg:”Error occured during add network”,
            statuscode:”356”, msg:”Network already exist please enter another SSID”,



            check ssid if already exist or not


    */

 
    public function validateAddNetwork($network_data){
    	
    	$error = array();
    	$publicprivate = array('1'=>'public','2'=>'private');
    	 
        if(!isset($network_data['user_id']) || empty($network_data['user_id'])){
        	$error['user_id'] = 'User id is required.';
        	$error['statuscode'] = 313;
        }else if(!is_numeric($network_data['user_id']) || empty($network_data['user_id'])){
        	$error['user_id'] = 'Invalid User ID.';
        	$error['statuscode'] = 317;
        }
        else if(!isset($network_data['name']) || empty($network_data['name'])){
        	$error['name'] = 'Network name is required.';
        	$error['statuscode'] = 317;
        }else if(!isset($network_data['ssid']) || empty($network_data['ssid'])){
        	$error['ssid'] = 'SSID is required.';
        	$error['statuscode'] = 318;
        }else if(!isset($network_data['network_type']) || empty($network_data['network_type'])){
        	$error['network_type'] = 'Network type is required.';
        	$error['statuscode'] = 354;
        }else if(!in_array($network_data['network_type'], $publicprivate)){
        	$error['network_type'] = 'Wrong network type,only private or public network accepted.';
        	$error['statuscode'] = 354;
        }
        else if(!isset($network_data['router_type']) || empty($network_data['router_type'])){
        	$error['router_type'] = 'Router type is required.';
        	$error['statuscode'] = 319;
        }
        /*else if(!isset($network_data['publicip']) || empty($network_data['publicip'])){
        	$error['publicip'] = 'Public Ip is required';
        	$error['statuscode'] = 320;
        }*/
       else if(!isset($network_data['netw_key']) || empty($network_data['netw_key'])){
        	$error['network_key'] = 'Network key is required.';
        	$error['statuscode'] = 321;
        }else if(!isset($network_data['conf_key']) || empty($network_data['conf_key'])){
        	$error['conf_key'] = 'Confirm key is required.';
        	$error['statuscode'] = 350;
        }else if(!isset($network_data['token']) || empty($network_data['token'])){
        	$error['token'] = 'Token can not be empty.';
        	$error['statuscode'] = 351;
        }else if(!isset($network_data['router_id']) || empty($network_data['router_id'])){
        	$error['router_id'] = 'Router id is required.';
        	$error['statuscode'] = 352;
        }
        else if(!is_numeric($network_data['router_id']) || empty($network_data['router_id'])){
        	$error['router_id'] = 'Invalid Router ID.';
        	$error['statuscode'] = 352;
        }else if($network_data['netw_key'] != $network_data['conf_key']){
        	$error['keyConfNotMatch'] = 'Network and confirm network key not matched.';
        	$error['statuscode'] = 322;
        }/*else if(filter_var($network_data['publicip'], FILTER_VALIDATE_IP) === false){
        	$error['notValidIp'] = 'Please enter valid Ip Address';
        	$error['statuscode'] = 353;
        }else if(!preg_match("/^[a-z A-Z0-9\\/\\\\.'\"]+$/",$network_data['ssid'])){
            $error['notValidssid'] = 'Please enter valid SSID,SSID can not contain special chareter(@,#,$,%,^,&,*,-)';
        	$error['statuscode'] = 355;

        }*/
    	return $error;

    }       
   
    public function addNetwork(){

    	$error=array();
    	$this->load->model('User');
    	$this->load->model('Network');
    	$response = array();
    	if(
    		$this->input->server('REQUEST_METHOD')=='POST'
          ){

             $error = $this->validateAddNetwork($_POST);
             if(!empty($error)&&count($error)>0){
              $response=$error;
             }else{

               // $error = $this->User->is_autorizedUser($_POST['user_id'],$_POST['token']);
             	$error = $this->User->isAuthorizedAdmin($_POST['user_id'],$_POST['token']);
                if($error['statuscode']===200){

                	$error = $this->Network->isSSISexist($_POST['user_id'],$_POST['ssid']);

                	if($error['statuscode']===200){
                      
                      $error = $this->Network->isRouterExist($_POST['router_id']);
                     
                       if($error['statuscode']===200){
                          $error = $this->Network->insertNetwork();
                           $response = $error;   
                         }else{
                         	$response = $error;   
                         }

                   }else{
                   	$response = $error;
                   }
                 
               }else{
       	     $response = $error;
           }
    }
             

	}else{

		$response['message'] = 'Invalid Request.';
	    $response['statuscode'] = 501;	

	}
	$this->output
	     ->set_content_type('application/json')
	     ->set_output(json_encode($response)); 

     
}

 /*----Start Add Network Api 11-January-2016 Evening End---------*/

 /*----------List and Search Network API-------Start---Date 12 Jan 2016----By Vipin chauhan----*/
   
    /*
       Method:- POST
    
       Request param: {user_id,search param(name-optional),token}

        Response Params:-{   error:false,statuscode:209,msg:”success”,’devices’ :”Connected device informations”}
           Validation error messages with status code:-
           statuscode:”313”, msg:”User id is required.”,
           statuscode:”316”, msg:”Unauthorized user..”,

    */


 public function listAndSearchNetwork(){
       
         $error = array();
         $response = array();
         $data = array();
         $this->load->model('Network');
         if(
         	 $this->input->server('REQUEST_METHOD')=='POST'
         	) {

         	       $error = $this->validatelistAndSearchNetwork($_POST);
                   if($error['statuscode']===200){
                         
                     $data = $this->Network->list_SearchNetwork();
                     
                      if(count($data)>0){
                        $response['error']='false';
                        $response['message'] = 'success';
                        $response['statuscode'] = 209;
                        $response['networkscount'] = count($data);
                        $response['networkList'] = $data;
                        

                      }else{
                          
                        $response['error']='false';
                        $response['message'] = 'success';
                        $response['statuscode'] = 209;
                        $response['networkscount'] = count($data);
                        $response['networkList'] = 'No device found with this admin.';
                        
                      }

                   }else{
                   	$response=$error;
                   }

         }else{
         	$response['message'] = 'Invalid Request.';
	        $response['statuscode'] = 501;	
         }

 $this->output
  ->set_content_type('application/json')
  ->set_output(json_encode($response)); 

 }

 function validatelistAndSearchNetwork($data){
 	 $error = array();
 	 $this->load->model('User');
 	 if(!isset($data['user_id']) || empty($data['user_id'])){
 	 	$error['user_id'] = 'User id is required.';
 	 	$error['statuscode'] = 313;
 	 }else if(!is_numeric($data['user_id']) || empty($data['user_id'])){
 	 	$error['user_id'] = 'Invalid User ID.';
 	 	$error['statuscode'] = 313;
 	 }

 	 else{
 	 	 
 	 	 		$error = $this->User->isAuthorizedAdmin($data['user_id'],$data['token']);

 	 	          
 	    }
  return $error;
 }





 /*----------List and Search Network API-------END---Date 12 Jan 2016----By Vipin chauhan----*/





 /*----Add New Device Start Date 13 jan 2016-----------------------------*/


  /*

        Method:- POST

   Request param: {netw_id, device_name, mobile_no, mac_id, device_id, email,token}
 
Validate mac Id:-
  ->Unique
->Formate(22-22-22-22-22-22)

  Response Params:-{error:false,statuscode:215,msg:”device has been added successfully.”}
          
 Validation error messages with status code:-
          statuscode:”316”, msg:”Unauthorized user..”,
          statuscode:”323”, msg:”Network id is required..”,
          statuscode:”324”, msg:”device name is required..”,
          statuscode:”325”, msg:”mobile no. is required..”,
          statuscode:”326”, msg:”mac_d. is required..”,
          statuscode:”327”, msg:”mac_d. is already exist.”,
          statuscode:”328”, msg:”mac_d. is not valid.”,
          statuscode:”329”, msg:”Network id does not recognize.”,
          statuscode:”330”, msg:”Error occured during add device.”,
          statuscode:”331”, msg:”This device already connected to this network”,



  */

   function is_valid_mac($mac)
   {
			  // 01:23:45:67:89:ab
			  if (preg_match('/^([a-fA-F0-9]{2}:){5}[a-fA-F0-9]{2}$/', $mac))
			    return true;
			  // 01-23-45-67-89-ab
			  if (preg_match('/^([a-fA-F0-9]{2}\-){5}[a-fA-F0-9]{2}$/', $mac))
			    return true;
			  // 0123456789ab
			  else if (preg_match('/^[a-fA-F0-9]{12}$/', $mac))
			    return true;
			  // 0123.4567.89ab
			  else if (preg_match('/^([a-fA-F0-9]{4}\.){2}[a-fA-F0-9]{4}$/', $mac))
			    return true;
			  else
			    return false;
 }       

 public function validateaddNewDevice($data){
    $error=array();
    $regphone = "/^[1-9][0-9]*/";
    if(!isset($data['network_id']) || empty($data['network_id'])){
    	$error['statuscode']=323;
    	$error['network_id']='Network id is required.';
      }else if(!is_numeric($data['network_id']) || empty($data['network_id'])){
        $error['statuscode']=323;
    	$error['network_id']='Invalid Network ID.';
      }else if(!is_numeric($data['user_id']) || empty($data['user_id'])){
        $error['statuscode']=323;
    	$error['network_id']='Invalid User ID.';
      }
      else if(!isset($data['device_name']) || empty($data['device_name'])){
        $error['statuscode']=324;
    	$error['device_name']='Device name is required.';
      }else if(!isset($data['mobile_no']) || empty($data['mobile_no'])){
        $error['statuscode']=325;
    	$error['mobile_no']='Mobile no is required.';
      }else if(!isset($data['mac_id']) || empty($data['mac_id'])){
        $error['statuscode']=326;
    	$error['mac_id']='Mac id is required.';
      }else if(!$this->is_valid_mac($data['mac_id'])){
        $error['statuscode']=328;
    	$error['mac_id']='Mac id is not valid.';
      }else if(!preg_match($regphone, $data['mobile_no'])){  
              		$error['statuscode']=409;
				    $error['msg']="Please enter valid phone number.";
      }else if(strlen($data['mobile_no'])!=10){  
              		$error['error']=true;
				    $error['statuscode']=408;
				    $error['msg']="Phone number should be 10 characters.";
     }
      else if(isset($data['email']) && !empty($data['email'])){  
          if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)){
              		$error['statuscode']=406;
				    $error['msg']="Please enter valid email.";
          }
      
      }
      

      if(count($error)==0){
            
         $this->load->model('User');
         $this->load->model('Device');
         $error = $this->User->isAuthorizedAdmin($data['user_id'],$data['token']);
         if($error['statuscode']===200){


             //$error = $this->Device->isNetworkIdExist($data['network_id']);
         	$error = $this->Device->isAuthorizeNetworkOwner($data['network_id'],$data['user_id']);

             if($error['statuscode']===200)
             {

             $error = $this->Device->isMacIdExist($data['mac_id'],$data['network_id']);
            
            
           }

         }    

      }
      //print_r($error);die;
      return $error;
 }
 



 public function addNewDevice(){

 	$error = array();
 	$this->load->model('Device');
 	$response = array();
 	if(
       $this->input->server('REQUEST_METHOD')=='POST'
	 ){
         $error = $this->validateaddNewDevice($_POST);
         
         if($error['statuscode']===200){

                  if(isset($error['mac_exist_not_this_network']) && $error['mac_exist_not_this_network'] === 200){
                       $error = $this->Device->insertDevice($error['mac_exist_not_this_network']);	
                  }else{
                    $error = $this->Device->insertDevice();
                  }
         $response = $error;	
                
         }else{
         	$response = $error;
         }
          
 	}else{

 		 $response['message'] = 'Invalid Request.';
	     $response['statuscode'] = 501;	

 	}
 	 $this->output
          ->set_content_type('application/json')
          ->set_output(json_encode($response)); 

 }	

/*-------------------------Add New Device End --------------------------------*/



/*------Get conneted device information Strat Here date 13 Jan 2016----*/


     /*
              Method:- POST
              Request param: {user_id,token}
              Response Params:-{   error:false,statuscode:208,msg:”success”,’devices’ :”Connected device informations”}
              Validation error messages with status code:-
              statuscode:”313”, msg:”User id is required.”,
              statuscode:”316”, msg:”Unauthorized user..”,



     */

              public function connetedDevices(){
              	$error = array();
              	$response = array();
                   
                    if($this->input->server('REQUEST_METHOD')=='POST'){
                       
                        $data = $_POST;
                     
                     if(!isset($data['user_id']) || empty($data['user_id'])){
                        $error['statuscode']=313;
                        $error['msg']='User id is required.';

                     }
                     if(!is_numeric($data['user_id']) || empty($data['user_id'])){
                        $error['statuscode']=313;
                        $error['msg']='Invalid User ID.';

                     }

                    if(isset($error) && !empty($error)){
                    	$response = $error;
                    }else{

                    	$this->load->model('User');
                        $this->load->model('Device');
                        $error = $this->User->isAuthorizedAdmin($data['user_id'],$data['token']);
                        if($error['statuscode']===200){

                        
                         $error = $this->Device->getDevice();
                         if(count($error)>0){
                         	$response['statuscode'] = 208;
                         	$response['message'] = 'success';
                         	$response['devicecount'] = count($error);
                         	$response['devices'] = $error;
                         }else{

                         	$response['statuscode'] = 208;
                         	$response['message'] = 'success';
                         	$response['devicecount'] = count($error);
                         	$response['devices'] = $error;

                         }


                        }else{
                        	$response = $error;
                        }
                    } 	
                         
                        

                    }else{
                    	$response['message'] = 'Invalid Request.';
	                    $response['statuscode'] = 501;	

                    }

     $this->output
          ->set_content_type('application/json')
          ->set_output(json_encode($response));
                  	
              }













/*-------Get Connected device information End here---------------------*/




/*-----Network Detail API------Start ---Date 13 Jan 2016-----*/
/*
    Network Detail Api:-
      Method:- POST

      Request param: {netw_id,token}
        Response Params:-{   error:false,statuscode:212,msg:”success.”,”network”:””}
           Validation error messages with status code:-
          statuscode:”316”, msg:”Unauthorized user..”,
          statuscode:”332”, msg:”Network id is required..”,
          statuscode:”333”, msg:”Token id is required.”,
          statuscode:”334”, msg:”This network not assiciated with this user.”,

*/




public function networkDetail(){

	    $error = array();
	    $data = array();
	    $response = array();
	    $this->load->model('Network');
	    if($this->input->server('REQUEST_METHOD')=='POST'){

	    	$error = $this->validatenetworkDetail($_POST);
	    	if($error['statuscode']===200){

                $data = $this->Network->get();

                            $response['statuscode'] = 212;
                         	$response['message'] = 'success';
                         	$response['networkcount'] = count($data);
                         	$response['networklist'] = $data[0];

	    		
	    	}else{
               
                $response = $error;

	    	}


        	     


	    }else{

	    	            $response['message'] = 'Invalid Request.';
	                    $response['statuscode'] = 501;	

	    }

	    $this->output
          ->set_content_type('application/json')
          ->set_output(json_encode($response));
}

   private function validatenetworkDetail($data){
   	
   	   $error = array();
   	   if(!isset($data['netw_id']) || empty($data['netw_id'])){
   	   	$error['statuscode']=332;
    	$error['network_id']='Network id is required.';
   	     }else if(!is_numeric($data['netw_id']) || empty($data['netw_id'])){
   	   	$error['statuscode']=332;
    	$error['network_id']='Invalid Network ID.';
   	   }
   	   else if(!isset($data['token']) || empty($data['token'])){
         
         $error['statuscode']=333;
    	$error['network_id']='Token is required.';

   	   }
   	   else{
   	   	    $this->load->model('User');

   	   	     $error = $this->User->isAuthorizedAdminByToken($data['token']);
   	   	     if($error['statuscode']==200){
   	   	     	$error = $this->User->isAdminAccessOwnNetwork($data['token'],$data['netw_id']);
                  
   	   	     }
   	    }
   

     return $error;
   }


/*------Network Detail API------End--------------------------*/




/*----Validate network API---------------Start here 14 Jan 2016--------*/


     /*

       12.Validate network API:-
          
          Method:- GET

            Request param: {netw_id}
            
            Validation error messages with status code:-
          
            statuscode:”335”, msg:”network does not exist.”,
            error:false,statuscode:213,msg:”success.”,”network”:”yes”





     */

            public function isNetworkExist(){

                $error = array();
                $response = array();
                $this->load->model('Network');
                if(
                	 $this->input->server('REQUEST_METHOD')=='GET'
            	  ){
                         $data = $_GET;
            	  	     if(
            	  	     	    !isset($data['netw_id']) 
            	  	     	    || empty($data['netw_id'])
                             	){

                        $response['message'] = 'Network id is required.';
	                    $response['statuscode'] = 332;	

            	  	     }else if(
            	  	     	         !is_numeric($data['netw_id'])
            	  	     	)
            	  	     {
            	  	     	 $response['message'] = 'Invalid Network ID.';
	                         $response['statuscode'] = 332;	

            	  	     }
            	  	     else{
            	  	     	  
                              $error = $this->Network->get($data['netw_id']);
                              if(count($error)>0){

                               $response['isnetworkexist'] = 'Yes';
	                           $response['statuscode'] = 335;	

                              }else{
                             
                               $response['isnetworkexist'] = 'No';
	                           $response['statuscode'] = 213;	   


                              }
                              
            	  	     }

                }else{

                	    $response['message'] = 'Invalid Request.';
	                    $response['statuscode'] = 501;	

                }

	        $this->output
	          ->set_content_type('application/json')
	          ->set_output(json_encode($response));

            }


     /*-----validate network API---------------End here----------------------*/


     

/*----Update Device------------Strat on jan 14 2016-------------------------*/



 /*

       Update Network API:-
          
          Method:- POST

            Request param: {netw_id}
            
            Validation error messages with status code:-
          
           error:false,statuscode:216,msg:”Device has been updated successfully.”

            statuscode:”336”, msg:”New Network id is required.”,
            statuscode:”337”, msg:”Old Network id is required.”,
            statuscode:”324”, msg:”Device name is required.”,
            statuscode:”325”, msg:”Mobile no is required.”
            statuscode:”326”, msg:”Mac id is required.”
            statuscode:”328”, msg:”Mac id is not valid.”
            statuscode:”409”, msg:”Please enter valid phone number.”
            statuscode:”408”, msg:”Phone number should be 10 characters.”
            statuscode:”406”, msg:”Please enter valid email.”
            statuscode:”420”, msg:”New Network id and Old network id can not be equal.”
            statuscode:”423”, msg:”Wrong old network id.”
            statuscode:”424”, msg:”Wrong device ID.”
            statuscode:”425”, msg:”User not Authorize to Access this network.”
            statuscode:”426”, msg:”This mac id already exist.please provide another mac id.”

            statuscode:”427”, msg:”Something went wrong while updating the device.”
            





     */


  
     public function updateDevice(){

     	$error = array();
     	$response = array();
     	if($this->input->server('REQUEST_METHOD')=='POST'){
         
            $error = $this->validateUpdateDevice($_POST);
            if($error['statuscode']===200){
                  $this->load->model('Device');
                  $error = $this->Device->updateDevice();
                   
                  $response = $error;
            }else{
            	$response = $error;

            }


     	}else{
     		$response['message'] = 'Invalid Request.';
	        $response['statuscode'] = 501;	

     	}
     	$this->output
          ->set_content_type('application/json')
          ->set_output(json_encode($response)); 

     }



 //   private function validateUpdateDevice($data){
    
 //    $error=array();
 //    $regphone = "/^[1-9][0-9]*/";
 //    if(!isset($data['new_network_id']) || empty($data['new_network_id'])){
 //    	$error['statuscode']=336;
 //    	$error['new_network_id']='New Network id is required.';
 //      }else if(!is_numeric($data['new_network_id']) || empty($data['new_network_id'])){
 //        $error['statuscode']=337;
 //    	$error['new_network_id']='Invalid New Network ID.';
 //      }else if(!isset($data['old_network_id']) || empty($data['old_network_id'])){
 //        $error['statuscode']=337;
 //    	$error['old_network_id']='Old Network id is required.';
 //      }else if(!is_numeric($data['old_network_id']) || empty($data['old_network_id'])){
 //        $error['statuscode']=337;
 //    	$error['old_network_id']='Invalid Old Network ID.';
 //      }else if(!isset($data['device_name']) || empty($data['device_name'])){
 //        $error['statuscode']=324;
 //    	$error['device_name']='Device name is required.';
 //      }else if($data['new_network_id'] == $data['old_network_id']){
 //        $error['statuscode']=420;
 //    	$error['device_name']='New Network id and Old network id can not be equal.';
 //      }else if(!isset($data['mobile_no']) || empty($data['mobile_no'])){
 //        $error['statuscode']=325;
 //    	$error['mobile_no']='Mobile no is required.';
 //      }else if(!isset($data['mac_id']) || empty($data['mac_id'])){
 //        $error['statuscode']=326;
 //    	$error['mac_id']='Mac id is required.';
 //      }else if(!$this->is_valid_mac($data['mac_id'])){
 //        $error['statuscode']=328;
 //    	$error['mac_id']='Mac id is not valid.';
 //      }else if(!is_numeric($data['user_id']) || empty($data['user_id'])){
 //        $error['statuscode']=338;
 //    	$error['user_id']='Invalid New User ID.';
 //      }else if(!preg_match($regphone, $data['mobile_no'])){  
 //              		$error['statuscode']=409;
	// 			    $error['msg']="Please enter valid phone number.";
 //      }else if(strlen($data['mobile_no'])!=10){  
 //              		$error['error']=true;
	// 			    $error['statuscode']=408;
	// 			    $error['msg']="Phone number should be 10 characters.";
 //     }
 //      else if(isset($data['email']) && !empty($data['email'])){  
 //          if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)){
 //              		$error['statuscode']=406;
	// 			    $error['msg']="Please enter valid email.";
 //          }
      
 //      }
      

 //      if(count($error)==0){
            
 //         $this->load->model('User');
 //         $this->load->model('Device');
 //         $error = $this->User->isAuthorizedAdmin($data['user_id'],$data['token']);
 //         if($error['statuscode']===200){


 //             $error = $this->Device->isAuthorizeNetworkOwner($data['new_network_id'],$data['user_id'],$data['id']);

             

 //             if($error['statuscode']===200)
 //             {

           

 //               $error = $this->Device->isOldNetworkIdExist($data['old_network_id']);

                
 //               if($error['statuscode']===200){

 //                $error = $this->Device->IdExist($data['id']);
                
 //                  if($error['statuscode']===200){
                  	 
 //                       $error = $this->Device->checkMac_idExist($data['id'],$data['user_id'],$data['mac_id']);

 //                       if( $error['statuscode']===200){
 //                       	$error = $this->Device->isAuthorizeNetworkOwner($data['old_network_id'],$data['user_id']);

 //                      }


                       	
 //                     }
                  	
                  	
                  
 //               } 
            
            
 //           }

 //         }    

 //      }
     
 //      return $error;
 // }
 
 
 
   private function validateUpdateDevice($data){
    
    $error=array();
    $regphone = "/^[1-9][0-9]*/";
    if(!isset($data['new_network_id']) || empty($data['new_network_id'])){
    	$error['statuscode']=336;
    	$error['new_network_id']='New Network id is required.';
      }else if(!is_numeric($data['new_network_id']) || empty($data['new_network_id'])){
        $error['statuscode']=337;
    	$error['new_network_id']='Invalid New Network ID.';
      }else if(!isset($data['old_network_id']) || empty($data['old_network_id'])){
        $error['statuscode']=337;
    	$error['old_network_id']='Old Network id is required.';
      }else if(!is_numeric($data['old_network_id']) || empty($data['old_network_id'])){
        $error['statuscode']=337;
    	$error['old_network_id']='Invalid Old Network ID.';
      }else if(!isset($data['device_name']) || empty($data['device_name'])){
        $error['statuscode']=324;
    	$error['device_name']='Device name is required.';
      }else if($data['new_network_id'] == $data['old_network_id']){
        $error['statuscode']=420;
    	$error['device_name']='New Network id and Old network id can not be equal.';
      }else if(!isset($data['mobile_no']) || empty($data['mobile_no'])){
        $error['statuscode']=325;
    	$error['mobile_no']='Mobile no is required.';
      }else if(!isset($data['mac_id']) || empty($data['mac_id'])){
        $error['statuscode']=326;
    	$error['mac_id']='Mac id is required.';
      }else if(!$this->is_valid_mac($data['mac_id'])){
        $error['statuscode']=328;
    	$error['mac_id']='Mac id is not valid.';
      }else if(!is_numeric($data['user_id']) || empty($data['user_id'])){
        $error['statuscode']=338;
    	$error['user_id']='Invalid New User ID.';
      }else if(!preg_match($regphone, $data['mobile_no'])){  
              		$error['statuscode']=409;
				    $error['msg']="Please enter valid phone number.";
      }else if(strlen($data['mobile_no'])!=10){  
              		$error['error']=true;
				    $error['statuscode']=408;
				    $error['msg']="Phone number should be 10 characters.";
     }
      else if(isset($data['email']) && !empty($data['email'])){  
          if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)){
              		$error['statuscode']=406;
				    $error['msg']="Please enter valid email.";
          }
      
      }






     if($error['statuscode'] ==420){
       $error['statuscode'] = 200;
     } 
        else if(count($error)==0){
            
         $this->load->model('User');
         $this->load->model('Device');
         $error = $this->User->isAuthorizedAdmin($data['user_id'],$data['token']);
         if($error['statuscode']===200){


             $error = $this->Device->isAuthorizeNetworkOwner($data['new_network_id'],$data['user_id'],$data['id']);

             

             if($error['statuscode']===200)
             {

           

               $error = $this->Device->isOldNetworkIdExist($data['old_network_id']);

                
               if($error['statuscode']===200){

                $error = $this->Device->IdExist($data['id']);
                
                  if($error['statuscode']===200){
                  	 
                       $error = $this->Device->checkMac_idExist($data['id'],$data['user_id'],$data['mac_id']);

                       if( $error['statuscode']===200){
                       	$error = $this->Device->isAuthorizeNetworkOwner($data['old_network_id'],$data['user_id']);

                      }


                       	
                     }
                  	
                  	
                  
               } 
            
            
           }

         }    

      }














     
      return $error;
 }
 
 







/*----Update Device-------------End-----------------------------------------*/







/*----Update Device-------------End-----------------------------------------*/




		/*------------------------Get Router api start-------------------------*/
		/*Method:- POST

		Request param: {token}
		   Response Params:-{   error:false,statuscode:211,msg:”success.”,”routers”:””}
		            Validation error messages with status code:-
		            statuscode:”316”, msg:”Unauthorized user..”, 
		            statuscode:"428", msg:"Router does not exist."		         
   	
   	   		          */

				public function getRouter()
				{
					$error=array();
					$response=array();
					$data=array();
					$this->load->model('Router');
					if($this->input->server('REQUEST_METHOD')=='POST')
					{
						$error=$this->validateRouterdetail($_POST);
						if($error['statuscode']===200)
						{
							$error=$this->Router->getRouterdetail();
							if(count($error)>0)
							{
								$response['statuscode']=211;
								$response['error']='false';
								$response['message']='success';
								$response['routercount']=count($error);
								$response['routers']=$error;
							}
							else
							{
								$response['statuscode']=211;
								$response['error']='false';
								$response['message']='success';
								$response['routercount']=count($error);
								$response['routers']=$error;
							}

						}
						else{
							$response=$error;
						}	
					}
					else{
		     		$response['message'] = 'Invalid Request.';
			        $response['statuscode'] = 501;	

		     		}
		     		$this->output
			          ->set_content_type('application/json')
			          ->set_output(json_encode($response));

				}

				public function validateRouterdetail($data)
				{
					$error=array();
					if(!isset($data['token']) || empty($data['token']))
					{
						$error['statuscode']=333;
    					$error['network_id']='Token is required.';
					}
					 else{
   	   	                $this->load->model('User');
   	   	                $error = $this->User->isAuthorizedAdminByToken($data['token']);
   	   	              	}
					return $error;
				}

		/*----------------------Get Router api code end------------------------------*/

/*-----------------------Api for Recent alert code start--------------------*/

		/* Request Parm:- {user_id,token}
                Response Params:-{error:false,statuscode:218,msg:”messages detail”}

            Validation error messages with status code:-
            statuscode:”313”, msg:”User id is required.”,
          statuscode:”316”, msg:”Unauthorized user.”, */

		public function recentAlert()
		{
			$error=array();
			$response=array();
			$this->load->model('Message');
			if($this->input->server('REQUEST_METHOD')=='POST')
			{
				$error=$this->validaterecent($_POST);
				if($error['statuscode']===200)
				{
					$error=$this->Message->getAlertdetail();
					if(count($error)>0)
					{						
					$response['statuscode']=218;
					$response['error']='false';
					$response['message']='success';
					$response['alertcount']=count($error);
					$response['messages_detail']=$error;
				    }
				    else
				    {
				    $response['statuscode']=218;
					$response['error']='false';
					$response['message']='success';
					$response['alertcount']=count($error);
					$response['messages_detail']=$error;
				    }

				}
				else
				{
				$response=$error;
				}

			}
			else
			{
		     	$response['message'] = 'Invalid Request.';
			    $response['statuscode'] = 501;
		     }

		     $this->output
			          ->set_content_type('application/json')
			          ->set_output(json_encode($response));


		}


		public function validaterecent($data)
		{
			$error=array();
			if(!isset($data['user_id']) || empty($data['user_id']))
			{
				 $error['statuscode']=313;
                 $error['msg']='User id is required.';
			}
			elseif (!isset($data['token']) || empty($data['token'])) 
			{				
				 $error['statuscode']=313;
                 $error['msg']='Token is required.';
			}
			else if(!is_numeric($data['user_id']) || empty($data['user_id'])){
   	   	        $error['statuscode']=313;
    	        $error['user_id']='Invalid User ID.';
   	            }
			else
			{
				$this->load->model('User');
   	   	        $error = $this->User->isAuthorizedAdmin($data['user_id'],$data['token']);
   	   	        
			}
			return $error;
		}





		/*--------------------recent alert code end------------------*/



		/*-----Delete Network By Admin Start here on 15 Jan 2016---------------*/


/*

   Delete Network :-
    Request param: {netw_id,token}
   Method:- POST

    Response Params:-{   error:false,statuscode:214,msg:”network has been deleted successfully.”}
           Validation error messages with status code:-
          statuscode:”316”, msg:”Unauthorized user..”,
          statuscode:”321”, msg:”Network id is required..”,
          statuscode:”441”, msg:”Something went wrong while delete network.”,


*/



public function deleteNetwork(){

  
     $error = array();
     $response = array();
     $this->load->model('User');
     $this->load->model('Device');
     $this->load->model('Network');
     if(
     	$this->input->server('REQUEST_METHOD')=='POST'
     	)
     {

           $data = $_POST;
		       if(
		       	   !isset($data['netw_id'])
		       	   || empty($data['netw_id'])
		       	){

		             $response['message'] = 'Network id is required.';
			         $response['statuscode'] = 321;	

		         }else if(
                   
                          !isset($data['token'])
		       	          || empty($data['token'])                    
 
                    ){

		         	       $response['message'] = 'Token is required.';
    	       	           $response['statuscode'] =313;	

		            }
		            else if(
                   
                          !is_numeric($data['netw_id'])
		       	          //|| empty($data['netw_id'])
		       	          || strlen(trim($data['netw_id'])) <= 0                    
 
                    ){

		         	       $response['message'] = 'Invalid network id.';
    	       	           $response['statuscode'] =321;	

		            }

		         else{

		         	$error = $this->User->isAuthorizedAdminByToken($data['token']);
		         	if($error['statuscode']===200){

                           
                          $error = $this->Device->isNetworkIdExist($data['netw_id']);  

                           if($error['statuscode']===200){


                            $error = $this->User->isAdminAccessOwnNetwork($data['token'],$data['netw_id']);

                            if($error['statuscode']===200){

                             $error = $this->Network->delete();
                             $response = $error;
                            }
		                 }
		              }
		              $response = $error;
		          }

              
 

     }else{ 
             $response['message'] = 'Invalid Request.';
	         $response['statuscode'] = 501;	
          }
 	$this->output
          ->set_content_type('application/json')
          ->set_output(json_encode($response)); 

}


/*-----Delete Network By Admin End--Here-------------*/

/*-------------Get alert message API ---Basically History---Start here---Date  15 Jan 2016------*/


  /*-------------Get alert message API ---Basically History---Start here---Date  15 Jan 2016------*/


  /*
       
         Get  Alert Message API:- 
            Method:- POST
statuscode:”217”, msg:”Please enter valid limit.”,
            Request Parm:- {user_id, limit,token}

             Validation error messages with status code:-
             statuscode:”313”, msg:”User id is required.”,
             statuscode:”316”, msg:”Unauthorized user.”,
             statuscode:”442”, msg:”limit is required.”,
             statuscode:”443”, msg:”Please enter valid limit.”,
             statuscode:”444”, msg:”invalid limit”,
              statuscode:”446”, msg:”No History Found.”,
               statuscode:”447”, msg:”success.”,

             



  */



 public function getHistoryAlert(){

         
         $response = array();
         $this->load->model('User');
         $this->load->model('Message'); 
         $this->load->library('session');
         $limit=0;
         $totalcount=0;
         $pagecount=0; 

         if(
         	 $this->input->server('REQUEST_METHOD')=='POST'
           ){

          
            $error = $this->validategetHistoryAlert($_POST);

              if(isset($error)&&count($error)>0){
              	$response = $error;
              }else{

                   $error = $this->User->isAuthorizedAdmin($_POST['user_id'],$_POST['token']);

                     if($error['statuscode'] === 200){


                     	$totalcount = $this->Message->getAllRecord();
                     	if($totalcount){

                       
                       $limit = $this->input->post('limit');
                       $pagecount = $this->input->post('page_count');
                       $localpagecount = ceil($totalcount/$limit);
                       //echo $localpagecount;die;
                       if($localpagecount<$pagecount){

                       	    $response['statuscode']=447;
                     	    $response['message'] = 'Invalid page count.';


                       }else{
                         
                               
                              if($pagecount==1){ 
                               $start = 0;
                               }else{
                               $start = ($pagecount-1)*$limit;
                           	   }
                           	   $error1 = $this->Message->getHistory($limit,$start);
                           	   if(count($error)>0){

                           	   	$response['statuscode'] = 447;
                           	   	$response['totalcount'] = $totalcount;
                           	   	$response['currentpage'] = $pagecount;
                           	   	$response['messagehistory'] = $error1;

                           	   }/*else{
                                  
                                $error['statuscode'] = 447;
                           	   	$error['totalcount'] = $totalcount;
                           	   	$error['currentpage'] = $pagecount;
                           	   	$error['messagehistory'] = $error;                           	   	
                           	   }*/


                       }




                     	}else{

                     	   $response['statuscode']=446;
                     	   $response['message'] = 'No History Found.';	
                     	}
                     }
                    

              }

          

         }else{

         	 $response['message'] = 'Invalid Request.';
	         $response['statuscode'] = 501;	

         }

        $this->output
          ->set_content_type('application/json')
          ->set_output(json_encode($response));  
 
 
 }



 /*public function setgetSession($limit){

 	
 	$this->load->library('session');
 	//$this->session->sess_destroy();
 	$session_limit = $this->session->userdata('limit');
    $limit_total = $session_limit+$limit;
    $this->session->set_userdata('limit', $limit_total);
    //$this->session->sess_destroy();
    return $limit_total;
 
 }
*/
  private function validategetHistoryAlert($data){
          
             $error = array();
             if(!isset($data['user_id']) || empty($data['user_id'])){

             	$error['message'] = 'User id is required';
    	       	$error['statuscode'] = 	313;
               }else if(!is_numeric($data['user_id']) || strlen($data['user_id']<=0)){
               	$error['message'] = 'Invalid user id.';
    	       	$error['statuscode'] = 	313;
               }
               else if(!isset($data['token']) || empty($data['token'])){

               	$error['statuscode']=333;
    	        $error['message']='Token is required.';
                }else if(!isset($data['limit']) || empty($data['limit'])){
                $error['statuscode']=442;
    	        $error['limit']='Limit is required.';
                }else if(!is_numeric($data['limit']) || strlen($data['limit']<=0)){
               	$error['message'] = 'Invalid limit.';
    	       	$error['statuscode'] = 	442;
               }
                else if(!$this->is_decimal($data['limit'])){
                 $error['statuscode']=443;
    	         $error['message']='Please enter valid limit.';
              }
               else if(!isset($data['page_count']) || empty($data['page_count'])){
                 $error['statuscode']=445;
    	         $error['message']='Page count is required.';
              } else if(!is_numeric($data['page_count']) || empty($data['page_count'])){
                 $error['statuscode']=445;
    	         $error['message']='Please enter valid page count.';
              }
                return $error;
  }

  function is_decimal( $val )
    {

         return is_numeric( $val ) && floor( $val ) == $val;
    }


/*--------------Get alert message API----End here------------------------------------------------*/



/*-----------------------Api for Recent activity code start--------------------*/

		/* Request Parm:- {user_id,token}
                Response Params:-{error:false,statuscode:218,msg:”messages detail”}

            Validation error messages with status code:-
            statuscode:”313”, msg:”User id is required.”,
          statuscode:”316”, msg:”Unauthorized user.”, */

		public function recentActivity()
		{
			$error=array();
			$response=array();
			$this->load->model('Activity');
			if($this->input->server('REQUEST_METHOD')=='POST')
			{
				$error=$this->validateactivity($_POST);
				if($error['statuscode']===200)
				{
					$error=$this->Activity->getActivitydetail($_POST['user_id']);
					if(count($error)>0)
					{						
					$response['statuscode']=207;
					$response['error']='false';
					$response['message']='success';
				    $response['activities']=$error;
				    }
				    else
				    {
				    $response['statuscode']=207;
					$response['error']='false';
					$response['message']='success';
					$response['activities']=$error;
				    }

				}
				else
				{
				$response=$error;
				}

			}
			else
			{
		     	$response['message'] = 'Invalid Request.';
			    $response['statuscode'] = 501;
		     }

		     $this->output
			          ->set_content_type('application/json')
			          ->set_output(json_encode($response));


		}


		public function validateactivity($data)
		{
			$error=array();
			if(!isset($data['user_id']) || empty($data['user_id']))
			{
				 $error['statuscode']=313;
                 $error['msg']='User id is required.';
			}
			elseif (!isset($data['token']) || empty($data['token'])) 
			{				
				 $error['statuscode']=313;
                 $error['msg']='Token is required.';
			}

			else if(!is_numeric($data['user_id']) || empty($data['user_id'])){
   	   	        $error['statuscode']=313;
    	        $error['user_id']='Invalid User ID.';
   	            }
   	        else if(isset($data['mac_id']) && !empty($data['mac_id']))
   	        {

		   	 if(!$this->is_valid_mac($data['mac_id'])){
		        $error['statuscode']=328;
		    	$error['mac_id']='Mac id is not valid.';
		      }
		      else{
   	        	$this->load->model('Activity');
   	   	        $error = $this->Activity->is_autorizedactiveUser($data['user_id'],$data['token']);
   	   	        if($error['statuscode']==200)
   	   	        {
   	   	        $error =$this->Activity->macidExist($data['mac_id']);
   	   	        }
   	   	       }
   	        }

			else
			{
				$this->load->model('User');
   	   	        $error = $this->User->isAuthorizedAdmin($data['user_id'],$data['token']);

			}
			return $error;
		}





		/*--------------------recent activity code end------------------*/


		

/*-------------delete history start from here-----Date 18 jan 2016-----------------------*/
/*
   
         Method:- POST

           Request Parm:- {msg_id,token}
                  Response Params:-{error:false,statuscode:222,msg:”history has been deleted successfully.”}

                  statuscode:”448”, msg:”Message id is required.”,


*/

public function delete_history(){

       $error = array();
       $response = array();
       if($this->input->server('REQUEST_METHOD')=='POST'){
           $data = $_POST;
       	if(!isset($data['msg_id']) 
       		|| empty($data['msg_id'])){


       		  $response['message'] = 'Message id is required.';
       		  $response['statuscode'] = 448;
       	}else if(!isset($data['token']) 
       		|| empty($data['token'])){

       		  $response['message'] = 'Token is required.';
    	      $response['statuscode'] =313;

       	}else if(!is_numeric($data['msg_id']) 
       		|| empty($data['msg_id'])){

       		  $response['message'] = 'Invalid message id.';
    	      $response['statuscode'] =313;

       	}else{

             $this->load->model('User');
             $this->load->model('Message');
             
             $error = $this->Message->isMessageExist($data['msg_id']);
                
              
           if($error['statuscode']==200)
           {
                     $error =  $this->User->isAuthorizedAdminByToken($data['token']);
		             


                     if($error['statuscode']==200)
                     {   
                      
                      $error = $this->User->isAuthorizedMessageByAdmin($data['token'],$data['msg_id']);
                       

		             if($error['statuscode']==200){

		             	$error = $this->Message->eraseMessage($data['msg_id']);


                        if($error['statuscode']==200){
                           
                            $response = $error;

                        }


		             } 
		         }

		    }         
         
          $response = $error;

       	}




       }else{

        $response['message'] = 'Invalid Request.';
	         $response['statuscode'] = 501;	

         }


        $this->output
          ->set_content_type('application/json')
          ->set_output(json_encode($response));  

   

}
/*-------------delete history start end here----------------------------*/

/* -------------Search and validate Network API ---Start from here-On-Date 18 jan 2016-----*/

 
/*  
    * User can search the network based on message.


*/

    /*
           
   
           * ----------For the time being----------Network Id does not required here-------------
             statuscode:”322”, msg:”network id is required.”,
             Request Parm:- {user_id, netw_id,token}









            3.  Search And validate network:-
         Method:- POST

         Request Parm:- {user_id,token}
         Response Params:-{error:false,statuscode:220,msg:”networks detail”}

            Validation error messages with status code:-
             statuscode:”313”, msg:”User id is required.”,
             statuscode:”316”, msg:”Unauthorized user.”,
             


 
    */



  public function searchAndValidateNetwork(){


         $error = array();
         $response = array();
        
         $this->load->model('User');
         $this->load->model('Network');
         
         if($this->input->server('REQUEST_METHOD')=='POST'){

                $data = $_POST;

                 $error = $this->validate_searchNetwork($data);
                 if($error['statuscode']==200){
                 
                 
                 $error = $this->Network->searchNetworkListNetwork();
                 
                 if(count($error)>0){


                 	$response['statuscode'] = 220;
                 	$response['message'] = 'success';
                 	$response['networkListCount'] = count($error);
                 	$response['networkList'] = $error;

                        

                 }else{
                 	
                 	$response['statuscode'] = 220;
                 	$response['message'] = 'success';
                 	$response['networkListCount'] = count($error);
                 	$response['networkList'] = $error;

                    }
                      


                 }else{
                 	$response = $error;
                 }
                  
         
         }else{

        $response['message'] = 'Invalid Request.';
	         $response['statuscode'] = 501;	

         }


        $this->output
          ->set_content_type('application/json')
          ->set_output(json_encode($response));   
          
       





  }

  private function validate_searchNetwork($data){

       $error = array(); 
       $this->load->model('User');
       if(!isset($data['user_id']) || empty($data['user_id'])){
   	        $error['message'] = 'User id is required';
	       	$error['statuscode'] = 	313;
       
       }else if(!is_numeric($data['user_id']) || empty($data['user_id'])){

       		$error['user_id'] = 'Invalid User ID.';
        	$error['statuscode'] = 317;

       }else if(!isset($data['token']) || empty($data['token'])){
     
            $response['message'] = 'Token is required.';
    	    $response['statuscode'] =313;

       }
       if(count($error)==0){
          
           $error = $this->User->is_autorizedUser($data['user_id'],$data['token']);
       	   
       }


  return  $error;



       

       

  }







 /*----------------Search and validate Network API---end Here------------------------------- */









 /*---------Start From here for Get Recent Network  Start here on date 18 jan 2016-----------------*/

/*
 
         2. Get Recent Network (connected and in range)



*/

 /*


               Method:- POST

             Request Parm:- {user_id,netw_id,token}
             Response Params:-{error:false,statuscode:219,msg:”networks detail”}

            Validation error messages with status code:-
             statuscode:”313”, msg:”User id is required.”,
             statuscode:”316”, msg:”Unauthorized user.”,
             statuscode:”322”, msg:”network id is required.”,



 */


/*This API will return the 10 recently connected networks with this user.
 




*/






  public function getRecentNetwork(){

  
           $error = array();
           $response = array();
           $this->load->model('Network');
   if($this->input->server('REQUEST_METHOD')=='POST'){


         $data = $_POST;

         $error = $this->validate_searchNetwork($data);

         if($error['statuscode']==200){

             
                $error = $this->Network->getRecentTenConnectedInfo();

                if(count($error)>0){


                 	$response['statuscode'] = 219;
                 	$response['message'] = 'success';
                 	$response['recentNetworkListCount'] = count($error);
                 	$response['recentNetworkList'] = $error;

                        

                 }else{
                 	
                 	$response['statuscode'] = 219;
                 	$response['message'] = 'success';
                 	$response['recentNetworkListCount'] = count($error);
                 	$response['recentNetworkList'] = $error;

                    } 




         }else{
         	$response = $error;
         }
         



   }else{
             $response['message'] = 'Invalid Request.';
	         $response['statuscode'] = 501;	

        }


        $this->output
          ->set_content_type('application/json')
          ->set_output(json_encode($response));   
                           
                        



  }


/*---------End here Get Recent Network----*******************-----------------------------------*/




	/*-----Remove Device By Admin Start here on 19 Jan 2016---------------*/


/*

   Method:- POST

     Request param: {device_id, netw_id, user_id}
     Response Params:-{error:false,statuscode:217,msg:”device has been deleted successfully.”}
          
          Validation error messages with status code:-
          statuscode:”316”, msg:”Unauthorized user..”,
          statuscode:”328”, msg:”device id is not valid.”,
          statuscode:”431”, msg:”Network id and device id does not recognize.”,
          statuscode:”430”, msg:”Device id does not recognize.”


*/



public function removeDevice(){
 
     $error = array();
     $response = array();
     $this->load->model('User');
     $this->load->model('Device');
     $this->load->model('Network');
     if(
     	$this->input->server('REQUEST_METHOD')=='POST'
     	)
     {
           $data = $_POST;
		       if(
		       	   !isset($data['netw_id'])
		       	   || empty($data['netw_id'])
		       	){

		             $response['message'] = 'Network id is required.';
			         $response['statuscode'] = 321;	

		         }  
		         else if(!isset($data['user_id']) || empty($data['user_id']))
		         {
   	       			 $response['message'] = 'User id is required';
	       			 $response['statuscode'] = 	313;
       
       			}
       			else if(!is_numeric($data['user_id']) || empty($data['user_id']))
       			{
       				$response['user_id'] = 'Invalid User ID.';
        			$response['statuscode'] = 317;

       			}

       			else if(!isset($data['device_id']) || empty($data['device_id']))
		         {
   	       			 $response['message'] = 'Device id is required';
	       			 $response['statuscode'] = 	313;
       
       			}
       			else if(!is_numeric($data['device_id']) || empty($data['device_id']))
       			{
       				$response['user_id'] = 'Invalid Device ID.';
        			$response['statuscode'] = 328;

       			}
		            else if(
                   
                          !is_numeric($data['netw_id'])
		       	          //|| empty($data['netw_id'])
		       	          || strlen(trim($data['netw_id'])) <= 0                    
 
                    ){

		         	       $response['message'] = 'Invalid network id.';
    	       	           $response['statuscode'] =321;	

		            }

		         else{

		         	$error = $this->is_autorizedonlyadmin($data['user_id']);
		         	if($error['statuscode']===200){
                          $error = $this->Device->isAdminAccessOwnDevice($data['user_id'],$data['device_id']);
                        
                          if($error['statuscode']===200)
                          	{
                          		$error = $this->Device->isAdminAccessOwnNet($data['user_id'],$data['netw_id']);  
                                              		
                          	if($error['statuscode']===200)
                          	{
                          		$error = $this->Device->isNetanddeviceIdExist($data['device_id'],$data['netw_id']);  
                                if($error['statuscode']===200)
		                          	{
		                          	$error = $this->Device->removenetDevice($data['device_id'],$data['netw_id']);
		                             	}
                          		
                          	}
                          	
                            }
		                 
		              }
		              $response = $error;
		          }
         
 

     }else{ 
             $response['message'] = 'Invalid Request.';
	         $response['statuscode'] = 501;	
          }
 	$this->output
          ->set_content_type('application/json')
          ->set_output(json_encode($response)); 

}


       public function is_autorizedonlyadmin($userid){

             $error = array();
             $array = array('id' => $userid,'role'=>'admin');
             $data = $this->db->get_where('users', $array)->result();
             
             if(count($data)==0)
             { 
              $error['statuscode'] = 316;
              $error['message'] = 'Unautorized user.'; 
              
             }else{
                 
                $error['statuscode'] = 200;
                

             }
             return $error;

       }
       

/*-----Remove device By Admin End--Here-------------*/


 /*

         statuscode:”432”, msg:”invalid user id.”,
         statuscode:”433”, msg:”Password has been updated successfully.”,
         statuscode:”434”, msg:”Something went wrong while updating the password.”,

        
  */
  

/*------update password API start here---on date 19 jan 2016------------------------------*/


  public function updatePassword(){

      $error = array();
      $response = array();
      if($this->input->server('REQUEST_METHOD') == 'POST'){
          $data = $_POST;
      	  $error = $this->validate_updatePassword($data);

          if($error['statuscode']==200){

          	 $this->load->model('User');


          	 $error = $this->User->profileupdatePasswor($data['user_id'],$data['password']);
          	 

          }
        $response = $error;


      }else{
             $response['message'] = 'Invalid Request.';
	         $response['statuscode'] = 501;	

        }


        $this->output
          ->set_content_type('application/json')
          ->set_output(json_encode($response));   
                           


  }
   private function validate_updatePassword($data){

  	  $error = array();

  	  if(!isset($data['user_id']) || empty($data['user_id'])){
  	  	$error['statuscode']=432;
  	  	$error['message']='user id is required.';
  	  }else if(!is_numeric($data['user_id']) || strlen($data['user_id'])<=0){
        $error['statuscode']=432;
  	  	$error['message']='invalid user id.';
  	  }else if(!isset($data['password']) || empty($data['password'])){
        $error['statuscode']=416;
	    $error['message']="Password is required.";
  	  }else if(!isset($data['confirm_password']) || empty($data['confirm_password'])){
        $error['statuscode']=416;
	    $error['message']="Confirm Password is required.";
  	  }else if(strlen($data['password'])<2 || strlen($data['password'])>12){
        $error['statuscode']=416;
		$error['message']="Password should be 2-12 chartered long.";
  	  }
  	  else if($data['confirm_password'] != $data['password']){
        $error['statuscode']=417;
		$error['message']="Password and Confirm Password does not matched.";
  	  }
  	  if(count($error)==0){


            $this->load->model('User');
           //$error = $this->common->validate_password($data['password']); 
           $error = $this->User->getUser($data['user_id']);
           
           //if($error['statuscode']==200){
           	
           	
           	  
           	   if(count($error)>0){
           	     $error['statuscode']=200; 	
           	   }else{
           	   	  $error['statuscode']=403; 	
           	   	  $error['message']='This user does not exist.'; 	
           	   }

           //} 




  	  }
      return $error;



  } 


/*------update password API end here---on date 19 jan 2016------------------------------*/



/*------Send Notification here-- Start-----------------------------------*/



/*

  after Add successfully device OR lets say connect network

  Send Push notification API:-


 Method:- POST

   Request Parm:- {to_user_id, from_user_id,status,message,netw_id,token}
                  Response Params:-{error:false,statuscode:222,msg:”network has been deleted successfully.”}
                  Validation error messages with status code:-
                 statuscode:”313”, msg:”to user id is required.”,
                 statuscode:”316”, msg:”Unauthorized user.”,
                 statuscode:”332”, msg:”from user id is required”,
                 statuscode:”430”, msg:”Status either connected or disconnected accepted.”,
                 statuscode:”431”, msg:”Device id is required”,


*/

public function validate_connectPushNotification($data){
         
          $error = array();
          $status = array('1'=>'connected','2'=>'disconnected');
          if(!isset($data['to_user_id']) || empty($data['to_user_id'])){
          	$error['statuscode'] = 313;
          	$error['message'] = 'Network add recipient id is required.';
          }else if(!isset($data['from_user_id']) || empty($data['from_user_id'])){
          	$error['statuscode'] = 313;
          	$error['message'] = 'Admin id is required.';
          }else if(!is_numeric($data['to_user_id']) || strlen($data['to_user_id'])<=0){

          	$error['statuscode'] = 313;
          	$error['message'] = 'Invalid Network add recipient id.';

          }
          else if(!is_numeric($data['from_user_id']) || strlen($data['from_user_id'])<=0){

          	$error['statuscode'] = 313;
          	$error['message'] = 'Invalid Admin id.';

          }else if(!isset($data['netw_id']) || strlen($data['netw_id'])<=0){

          	$error['statuscode'] = 322;
          	$error['message'] = 'Network id is required.';

          }
          else if(!is_numeric($data['netw_id']) || strlen($data['netw_id'])<=0){

          	$error['statuscode'] = 322;
          	$error['message'] = 'Invalid Network id.';

          }else if(!isset($data['status']) || strlen($data['status'])<=0){
               
                $error['statuscode'] = 430;
          	    $error['message'] = 'Status is required.';          	
          }
          else if(!in_array($data['status'], $status)){
               
                $error['statuscode'] = 430;
          	    $error['message'] = 'Status either connected or disconnected accepted.';          	
          }else if(!isset($data['token']) || strlen($data['token'])<=0){
               
                $error['message'] = 'Token is required.';
    	        $error['statuscode'] =313;          	
          }else if(!isset($data['device_id']) || strlen($data['device_id'])<=0){
               
                $error['message'] = 'Device id is required.';
    	        $error['statuscode'] =431;          	
          }
          if(count($error)==0){
             
                $this->load->model('User');

                $error = $this->User->isAuthorizedAdmin($data['from_user_id'],$data['token']);
                if($error['statuscode']==200){
                 
                   $error = $this->User->isAdminAccessOwnNetwork($data['token'],$data['netw_id']);
                    
                   if($error['statuscode']==200){
               	      
               	      $error = $this->User->getUserPush($data['to_user_id']);
                      
                      if(count($error)>0){
                      	$error['statuscode']=200;
                      }else{
                      	$error['statuscode']=340;
                      	$error['message']='Network add recipient does not exist.';
                      }    
                   
                   } 
                

                }

          }
           
          return $error;

 	  
 }

public function connectPushNotification(){

   $error = array();
   $response = array();

   if($this->input->server('REQUEST_METHOD')=='POST'){

          $data = $_POST; 
          $error = $this->validate_connectPushNotification($data);

          if($error['statuscode']==200){
            $this->load->model('Message');
            $error = $this->Message->insertMessage();
            
             if($error['statuscode']==200){


                 $messagedetil = $this->Message->getInfoStatus($data['to_user_id'],$data[
                 	'netw_id']);
                 
                  if($messagedetil['statuscode']==200){



                      $complete_message = array(
           
                          'connectedInfo'=>$messagedetil['user_detail']->name.' has been '.$data['status'].' To '.$messagedetil['network_detail']->networkName.' Network'
                      ); 

                        
                       $status = $this->send_gcm($complete_message,$data['device_id']);

                         if($status){

                              $error['statuscode']=200;
                              $error['message']='Push notification were sent successfully.';
                              $error['notifyinfo']=$complete_message;
                         }else{

                         	  $error['statuscode']=200;
                              $error['message']='Push notification were not sent successfully.';
                              $error['notifyinfo']='Please check device id.';
                         }







                  }

               
             }
              
           
               


          } 
          $response = $error;
         

   }else{

         	 $response['message'] = 'Invalid Request.';
	         $response['statuscode'] = 501;	

         }

        $this->output
          ->set_content_type('application/json')
          ->set_output(json_encode($response));  

}




 
public function send_gcm($messageInfo,$deviceId)
{
     
        $this->load->library('gcm');
 

        /*$this->gcm->setMessage('Hi android Team i am ready to send push notification '.date('d.m.Y H:s:i'));*/
        $this->gcm->addRecepient($deviceId);
        $this->gcm->setData(array(
            'message_key' => $messageInfo
        ));
 
        $this->gcm->setTtl(500);
        $this->gcm->setTtl(false);
        $this->gcm->setGroup('Test');
        $this->gcm->setGroup(false);
         if ($this->gcm->send())
        	return true;
         else
         	return false;
        	//return true;
        //else 
        	//return false;
        /*    echo 'Success for all messages';
        else
            echo 'Some messages have errors';
*/
    // and see responses for more info
      /* print_r($this->gcm->status);
       print_r($this->gcm->messagesStatuses);


    die(' Worked.');*/
}

/*------Send Push Notification --End-- Here--------------------------------*/


/*------------update profile api code start-----------*/

  public function updateProfile(){


         $error = array();
         $response = array();
        
         $this->load->model('User');
         $this->load->model('Network');
         
         if($this->input->server('REQUEST_METHOD')=='POST'){

                $data = $_POST;

                 $error = $this->validate_profile($data);
                 if($error['statuscode']==200){                 
                 
                 $error = $this->User->updateProfile();
                 
                 if($error['statuscode']==210){
                 	$response['statuscode'] = 220;
                 	$response['message'] = 'success';
                 	$response= $error;                        

                 }else{
                 	
                 	$response['statuscode'] = 220;
                 	$response['message'] = 'success';
                 	$response = $error;

                    }
                      


                 }else{
                 	$response = $error;
                 }
                  
         
         }else{

        $response['message'] = 'Invalid Request.';
	         $response['statuscode'] = 501;	

         }


        $this->output
          ->set_content_type('application/json')
          ->set_output(json_encode($response));   
          
       
  }

  private function validate_profile($data){

       $error = array(); 
       $regphone = "/^[1-9][0-9]*/";
	   $regphone1='/^[0-9]+$/';
       $this->load->model('User');
       if(!isset($data['user_id']) || empty($data['user_id'])){
   	        $error['message'] = 'User id is required';
	       	$error['statuscode'] = 	313;
       
       }else if(!is_numeric($data['user_id']) || empty($data['user_id'])){

       		$error['user_id'] = 'Invalid user ID.';
        	$error['statuscode'] = 317;

       }else if(!isset($data['token']) || empty($data['token'])){
     
            $error['message'] = 'Token is required.';
    	    $error['statuscode'] =313;

       }
        else if(!isset($data['name']) || empty($data['name'])){
     
            $error['message'] = 'Name is required.';
    	    $error['statuscode'] =313;

       }
       else if(!isset($data['phone']) || strlen(trim($data['phone'])) <= 0){
			
			$error['statuscode']=402;
			$error['msg']="Please enter the phone number.";

		}
       else if (strlen($data['phone'])!=10){
              	
			 $error['statuscode']=408;
		     $error['msg']="Phone number should be 10 characters.";

        }
       else if (!preg_match($regphone, $data['phone'])){
              		
		    $error['statuscode']=409;
		    $error['msg']="Please enter valid phone number.";

        }
       else if (!preg_match($regphone1, $data['phone'])){
              		
			$error['statuscode']=409;
			$error['msg']="Please enter valid phone number.";

        }
       else if(!isset($data['country_code']) || strlen(trim($data['country_code'])) <= 0){
			$error['statuscode']=407;
			$error['msg']="Country code is required in phone.";

		}
       else if(!isset($data['user_role']) || strlen(trim($data['user_role'])) <= 0){
			$error['statuscode']=305;
			$error['msg']="User role is required.";

		}
	  else if($this->User->isUserExistPhoneforupdate($data['user_role'],$data['country_code'].$data['phone'],$data['user_id'])){

             $error['error']=true;
			 $error['statuscode']=403;
			 $error['msg']="Phone number is already registered.";

		}
       else
       {
       	   $error = $this->User->isAuthorizedUserByToken($data['user_id'],$data['token']);
                  	   
       }


  return  $error;

  }




/*------------------update profile code end----------------*/










}
