<?php
class Device extends CI_Model{
  	function __construct(){
  		$this->load->database();
  	}

   
      public function isMacIdExist($macid,$network_id){
         
       $error = array();  
       $this->db->where('mac_id',$macid);
       $query = $this->db->get('devices');
        if($query->num_rows()==0){
          $error['statuscode']=200;
        }else{

           $isDeviceAlreadyConnected = $this->isdeviceAlreadyconnected($macid,$network_id);
           $error = $isDeviceAlreadyConnected;

           if($error['statuscode']===200){
           	  $error['statuscode'] = 200;
              $error['mac_exist_not_this_network'] = 200;
           }           

          /*$error['statuscode']=327;
          $error['message']='Mac id is already exist.';*/



        }
      return $error;
      }


  protected function isdeviceAlreadyconnected($macid,$network_id){
           
            $error = array();
            $this->db->select('id');  
            $this->db->where('mac_id',$macid);
            $query = $this->db->get('devices')->result();
            $device_id = $query[0]->id;

            
            
            $this->db->where('device_id',$device_id);
            $this->db->where('network_id',$network_id);
            $network_device = $this->db->get('network_device');
            //print_r($network_device);die;
            if($network_device->num_rows()!=0){
            	$error['statuscode']=331;
            	$error['message']='This device already connected to this network';

            }else{
            	$error['statuscode']=200;
            	
            }
           return $error; 

  }




      public function isNetworkIdExist($network_id){
         
       $error = array();  
       $this->db->where('id',$network_id);
       $query = $this->db->get('networks');
        if($query->num_rows()==0){
          
          $error['statuscode']=329;
          $error['message']='Network id does not recognize.';
          
        }else{
          
          $error['statuscode']=200;
        }
      return $error;
      }
      public function insertDevice($mac_exist_not_this_network=null){

        $this->load->helper('date');
        $error = array();
        
      if(isset($mac_exist_not_this_network) && $mac_exist_not_this_network==200){

            $this->db->select('id');  
            $this->db->where('mac_id',$this->input->post('mac_id'));
            $query = $this->db->get('devices')->result();
            $device_id = $query[0]->id;



            $data_for_network_device1 = array(
              
              'device_id' => $device_id,
              'network_id' => $this->input->post('network_id'),
            ); 

          $this->db->insert('network_device', $data_for_network_device1);   
         
          if($this->db->affected_rows()===1){
          
          $error['statuscode']=215;
          $error['error']='false';
          $error['message']='Device has been added successfully.';
          }else{

          	$error['statuscode']=330;
            $error['error']='true';
            $error['message']='Error occured during add device.';

          }

      }else{
      
        $data_for_device = array(
              'user_id' => $this->input->post('user_id'),
              'device_name' => $this->input->post('device_name'),
              'mobile' => $this->input->post('mobile_no'),
              'mac_id' => $this->input->post('mac_id'),
              'phone_id' => $this->input->post('phone_id'),
              'email' => $this->input->post('email'),
              'created_date' => date('Y-m-d H:i:s'),
              'updated_date' => date('Y-m-d H:i:s')

          );

           
         $this->db->insert('devices', $data_for_device); 
            
         if($this->db->affected_rows()===1){
          $insert_id = $this->db->insert_id();
          
          $data_for_network_device = array(
              
              'device_id' => $insert_id,
              'network_id' => $this->input->post('network_id'),
            ); 

          $this->db->insert('network_device', $data_for_network_device);   
         
          if($this->db->affected_rows()===1){
          
          $error['statuscode']=215;
          $error['error']='false';
          $error['message']='Device has been added successfully.';
          }else{

          	$error['statuscode']=330;
            $error['error']='true';
            $error['message']='Error occured during add device.';

          } 
         }else{

          $error['statuscode']=330;
          $error['error']='true';
          $error['message']='Error occured during add device.';

           }
       }










           return $error;

      }

       /*------Get conneted device information Strat Here date 13 Jan 2016----*/

     public function getDevice(){

        $data = array();
      $user_id = $this->input->post('user_id');


        $this->db->select('devices.*
                          ,network_device.network_id AS Network_id'

                    )
                ->from('devices')
                ->join('network_device', 'devices.id = network_device.device_id')
                ->where('devices.user_id',$user_id);
        $data = $this->db->get()->result();
       


      
      return $data; 

      } 
      /*-------Get Connected device information End here---------------------*/


         /*---update device ----Start---------here---------------*/
     public function isOldNetworkIdExist($network_id){


      $error = array();  
       $this->db->where('network_id',$network_id);
       $query = $this->db->get('network_device');
        if($query->num_rows()==0){
          
          $error['statuscode']=423;
          $error['message']='Wrong old network id.';
          
        }else{
          
          $error['statuscode']=200;
        }
      return $error;



     }

     public function IdExist($id){

         /*check here if user provide valid id of device table ID*/

         $error = array();  
       $this->db->where('id',$id);
       $query = $this->db->get('devices');
        if($query->num_rows()==0){
          
          $error['statuscode']=424;
          $error['message']='Wrong device ID.';
          
        }else{
          
          $error['statuscode']=200;
        }
      return $error;




     }

    public function isAuthorizeNetworkOwner($network_id,$user_id,$device_id){


              $error = array();
              $data = array(
                  'id'=>$network_id,
                  'user_id'=>$user_id

                );  
                 $this->db->where($data);
                 $query = $this->db->get('networks');
                  if($query->num_rows()==0){
                    
                    $error['statuscode']=425;
                    $error['message']='User not Authorize to Access this network.';
                    
                  }else{


                    $data = array(
                  'device_id'=>$device_id,
                  'network_id'=>$network_id

                );  
                 $this->db->where($data);
                 $query1 = $this->db->get('network_device');

                  if($query1->num_rows()>0){
                    
                    $error['statuscode']=425;
                    $error['message']='This new network id already connected with this device.please select another new network id.';
                  }else{
                    $error['statuscode']=200;
                  } 
                   
                  




                  }
                return $error;




     }

     public function checkMac_idExist($device_id,$user_id,$mac_id){

      


           $error = array();
              $data = array(
                  'id !='=>$device_id,
                  //'user_id !='=>$user_id,
                  'mac_id'=>$mac_id

                );  
                 $this->db->where($data);
                 $query = $this->db->get('devices');

                  if($query->num_rows()!=0){
                    
                    $error['statuscode']=426;
                    $error['message']='This mac id already exist.please provide another mac id.';
                    
                  }else{
                    
                    $error['statuscode']=200;
                  }
                return $error;








     }


  public function updateDevice(){
       
        $error = array();

            $update_data = array(
              
              'device_name' => $this->input->post('device_name'),
              'mobile' => $this->input->post('mobile_no'),
              'mac_id' => $this->input->post('mac_id'),
              'phone_id' => $this->input->post('phone_id'),
              'email' => $this->input->post('email'),
              'updated_date' => date('Y-m-d H:i:s')
             );

            $this->db->where('id', $this->input->post('id'));
            
            $this->db->update('devices', $update_data);
            
            if($this->db->affected_rows()!==0){
             
                $network_device = array(
                              
                               'device_id'=>$this->input->post('id'),                               
                               'network_id'=>$this->input->post('new_network_id')

                  );

                $condition = array(

                     'device_id'=>$this->input->post('id'),
                     'network_id'=>$this->input->post('old_network_id')
                            
                   );
               

                $this->db->where($condition);
                $this->db->delete('network_device');
              
              if($this->db->affected_rows())
              {   

               
                $this->db->insert('network_device', $network_device);

                 if($this->db->affected_rows()!==0){
                  
                   $error['statuscode']=216;
                   $error['message']='Device has been updated successfully.';
                 }else{
 
                   $error['statuscode']=427;
                   $error['message']='Something went wrong while updating the device.';
                  
                 }
               }else{


                            $condition1 = array(

                                 'device_id'=>$this->input->post('id'),
                                 'network_id'=>$this->input->post('new_network_id')
                                        
                               );

                            $this->db->where($condition1);
                            $query = $this->db->get('network_device');

                            if($query->num_rows()>0){
                             $this->db->where($condition1);
                             $this->db->delete('network_device');
                                
                              }
                  
                                  

                                $this->db->insert('network_device', $network_device);

                                 if($this->db->affected_rows())
                                  {

                                   $error['statuscode']=216;
                                   $error['message']='Device has been updated successfully.';

                                   }else{

                                  $error['statuscode']=427;
                                  $error['message']='Something went wrong while updating the device.';
                                  }
               }




            }else{
                  $error['statuscode']=427;
                  $error['message']='Something went wrong while updating the device.';
            } 

  return $error;

     }
     /*----update device ----end-----------here--------------*/


/*------------device and network exist code----------------*/
public function removenetDevice($device_id,$network_id)
{
$error=array();
$flag=false;
$flag = $this->deleteFromnetdevice($device_id,$network_id);
             
                  if($flag){                             
                             $error['statuscode'] = 217;
                             $error['message'] = 'Device has been remove successfully.'; 

                     }else{
                  
                        $error['statuscode'] = 441;
                        $error['message'] = 'Something went wrong while delete network.';
                     }                           

             return $error; 
}
private function deleteFromnetdevice($device_id,$network_id){

           $this->db->where('device_id',$device_id);
           $this->db->where('network_id',$network_id);
           if(
               $this->db->delete('network_device')
              ){
            return true;
           }else{
            return false;
           }

       }

    public function isNetanddeviceIdExist($device_id,$network_id){
         
       $error = array();  
       $this->db->where('network_id',$network_id);
       $this->db->where('device_id',$device_id);
       $query = $this->db->get('network_device');
        if($query->num_rows()==0){
          
          $error['statuscode']=431;
          $error['message']='Network id and device id does not recognize.';
          
        }else{
          
          $error['statuscode']=200;
        }
      return $error;
      }


public function isAdminAccessOwnNet($user_id,$network_id){

    $error = array();
      $query = $this->db->get_where('networks', array('id' => $network_id,'user_id' => $user_id));
       if($query->num_rows()===0){
        $error['statuscode']=334;
        $error['message'] = 'This network not associated with this user.';
       }else{
          $error['statuscode']=200;        
       }
        
       return $error;    

   } 

public function isAdminAccessOwnDevice($user_id,$device_id){

    $error = array();
      $query = $this->db->get_where('devices', array('id' => $device_id,'user_id' => $user_id));
       if($query->num_rows()===0){
        $error['statuscode']=432;
        $error['message'] = 'This device not associated with this user.';
       }else{
          $error['statuscode']=200;        
       }
        
       return $error;    

   } 

/*------------------device and network exist code end------------*/



  }
?>
