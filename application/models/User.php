<?php

 class User extends CI_Model{
   
      function __construct() {

        $this->load->database();
        $this->load->library('common');

       }
       
       /*public function getUserByEmail_Password($email,$password)
       {
             $error = array();
             $password = md5($password);  
             $array = array('email' => $email, 'password' => $password);
             $data = $this->db->get_where('users', $array)->result();
             
             if(count($data)==0)
             { 
              $error['statuscode'] = 421;
              $error['message'] = 'Invalid email or password.'; 
              
             }else{
                $resp=$this->common->UniqueKey();
                $this->update_Token($resp,$data[0]->id);
                $data[0]->token = $resp;
                $error['statuscode'] = 200;
                $error['data'] = $data[0];

             }
             return $error;

       	     
       
       }*/
       public function getUserByEmail_Password($email,$password,$role)
       {
             $error = array();
             $password = md5($password);  
             $array = array('email' => $email, 'password' => $password,'role' =>$role);
             $data = $this->db->get_where('users', $array)->result();
             
             if(count($data)==0)
             { 
              $error['statuscode'] = 421;
              $error['message'] = 'Invalid email or password.'; 
              
             }else{
                $resp=$this->common->UniqueKey();
                $this->update_Token($resp,$data[0]->id);
                $data[0]->token = $resp;
                $error['statuscode'] = 200;
                $error['data'] = $data[0];

             }
             return $error;

             
       
       }
       protected function update_Token($token,$id)
       {
                $data = array(
		               'token' => $token,
		              );
		$this->db->where('id', $id);
	        $this->db->update('users', $data);  
       }
      public function getUserById($userid,$token){

             $error = array();
             $array = array('id' => $userid, 'token' => $token);
             $data = $this->db->get_where('users', $array)->result();
             
             if(count($data)==0)
             { 
              $error['statuscode'] = 422;
              $error['message'] = 'Unautorized user.'; 
              
             }else{
                $this->make_Token_empty($data[0]->id);
                $error['statuscode'] = 200;
                $error['message'] = 'User has been logout successfully.';

             }
             return $error;

       }
       protected function make_Token_empty($id)
       {
               $data = array(
                   'token' => '',
                  );

        $this->db->where('id', $id);
        $this->db->update('users', $data);  
       }
       /* public function getUserByEmail($email)
       {
             $error = array();
             $array = array('email' => $email);
             $data = $this->db->get_where('users', $array)->result();
             
             if(count($data)==0)
             { 
              $error['statuscode'] = 304;
              $error['message'] = 'Email does not exist.'; 
              
             }else{
                $error['statuscode'] = 200;
                $error['data'] = $data[0];

             }
             return $error;

             
       
       }*/
       public function getUserByEmail($email,$role)
       {
             $error = array();
             $array = array('email' => $email,'role'=>$role);
             $data = $this->db->get_where('users', $array)->result();
             
             if(count($data)==0)
             { 
              $error['statuscode'] = 304;
              $error['message'] = 'Email does not exist.'; 
              
             }else{
                $error['statuscode'] = 200;
                $error['data'] = $data[0];

             }
             return $error;

             
       
       }
       public function update_and_getPassword($id){
        
        $password = $this->common->get_random_password();
        
        $encrypt_password = md5($password);

        
        $this->update_password($id,$encrypt_password);
         
        return $password; 
       
       }
         /*for update password start*/
         
           public function profileupdatePasswor($id,$password){
            
                $error = $this->update_password($id,md5($password));
                if($error){
                  $response['statuscode'] = 433;
                  $response['message'] = 'Password has been updated successfully.';
                }else{
                           
                  $response['statuscode'] = 434;
                  $response['message'] = 'Something went wrong while updating the password.';        
                }
                return $response;
           }

       /*for update password end*/

       protected function update_password($id,$password)
       {
           $data = array(
                   'password' => $password,
                  );

        $this->db->where('id', $id);
         $query = $this->db->update('users', $data);  
        if($query){
          return 1;
        }else{
          return 0;
        }  
       }
       /*===========For Add Network API===========Start============*/
       public function is_autorizedUser($userid,$token){

             $error = array();
             $array = array('id' => $userid, 'token' => $token,'role'=>'user');
             $data = $this->db->get_where('users', $array)->result();
             
             if(count($data)==0)
             { 
              $error['statuscode'] = 316;
              $error['message'] = 'Unautorized user.'; 
              
             }else{
                 
                $error['statuscode'] = 200;
                

             }
             return $error;

       }
       /*===========Form Add Netwrk API===========End==============*/

       /*=================List and Search Network API Start==============*/

       public function isAuthorizedAdmin($adminId,$token){

             $error = array();
             $array = array('id' => $adminId, 'token' => $token,'role'=>'admin');
             $data = $this->db->get_where('users', $array)->result();
             
             if(count($data)==0)
             { 
              $error['statuscode'] = 316;
              $error['message'] = 'Unautorized user.'; 
              
             }else{
                 
                $error['statuscode'] = 200;
                

             }
             return $error;

       }
  /*=================List and Search Network API End==============*/

    /*=========For Global Feedback send to Arrival Admin Start=============*/
   public function getUser($userid){

             $error = array();
             $array = array('id' => $userid);
             $data = $this->db->get_where('users', $array)->result();
             return $data;

       }
  /*=========For Global Feedback send to Arrival Admin End===============*/




  
  /*======================Get network By ID==========Start here===============*/
  public function isAuthorizedAdminByToken($token){

             $error = array();
             $array = array('token' => $token,'role'=>'admin');
             $data = $this->db->get_where('users', $array)->result();
             
             if(count($data)==0)
             { 
              $error['statuscode'] = 316;
              $error['message'] = 'Unautorized user.'; 
              
             }else{
                 
                $error['statuscode'] = 200;
                

             }
             return $error;

       }

   public function isAdminAccessOwnNetwork($token,$network_id){

    $error = array();

     $this->db->select('users.id')
              ->where('users.token',$token);
        $query = $this->db->get('users')->result();
        if(count($query)>0){
          $user_id = $query[0]->id;
        }
       $query1 = $this->db->get_where('networks', array('id' => $network_id,'user_id' => $user_id));
       if($query1->num_rows()===0){
        $error['statuscode']=334;
        $error['message'] = 'This network not associated with this user';
       }else{
          $error['statuscode']=200;        
       }
        
        return $error;      

    

   }    
  /*=======================Get network By Id=========End here==================*/


  /*

            Description

       :-Same email can be used for Admin and User

       Same email can be used for admin and user.
      : Same can not be used for more then one admin
      :- Same email can not be used for more then one user


*/

      /*----------check here only email and role--- Start-*/

  public function isUserExistEmail($email,$role)
  {
           
              $condition = array(
                         
                          'email'=>$email,
                          'role'=>$role
                       );
              $this->db->where($condition);
              $query = $this->db->get('users');
              if($query->num_rows()>0){
                    return true;
                  }else{  
                    return false;
                  }


  }
/*----------check here only phone and role-- End --*/

public function isUserExistPhone($role,$mobile)
  {
           
              $condition = array(
                         
                          'phone'=>$mobile,
                          'role'=>$role
                       );
              $this->db->where($condition);
              $query = $this->db->get('users');
              if($query->num_rows()>0){
                    return true;
                  }else{  
                    return false;
                  }


  }
/*----------check here only phone and role-- End --*/



/*---START---------Delete history---user Authorization based on token--------only----------*/
/* public function isAuthorizedUserByToken($token){

             $error = array();
             $array = array('token' => $token);
             $data = $this->db->get_where('users', $array)->result();
             
             if(count($data)==0)
             { 
              $error['statuscode'] = 316;
              $error['message'] = 'Unautorized user.'; 
              
             }else{
                 
                $error['statuscode'] = 200;
                

             }
             return $error;

       }
*/

       
/*---START---------Delete history---user Authorization based on token--------only----------*/
 public function isAuthorizedMessageByAdmin($token,$m_id){

             $error = array();
             $array = array('token' => $token);
             $data = $this->db->get_where('users', $array)->result();
              
             if(count($data)==0)
             {
              $error['statuscode'] = 316;
              $error['message'] = 'Unautorized user.'; 
              
             }else{
                 
                 $admin_id = $data[0]->id;


                 
                $array1 = array('from_user_id' => $admin_id,'id' => $m_id);
                $data1 = $this->db->get_where('messages', $array1)->result();    
                 
                if(count($data1)==0){
                    
                $error['statuscode'] = 316;
                $error['message'] = 'Admin not Authorize to delete this message.'; 

                }else{
                  $error['statuscode'] = 200;
                }                
  


                
                

             }
             return $error;

       }

/*-----END-------Delete history---user Authorization based on token--------only----------*/       



/*-----END-------Delete history---user Authorization based on token--------only----------*/  


/*----------------update profile----------------*/

public function updateProfile()
{
 $this->load->helper('date');
        $error = array();
        $user_id=$this->input->post('user_id');
        $mobile=$this->input->post('country_code').$this->input->post('phone');
         $data = array(
              'name' => $this->input->post('name'),
              'phone'=> $mobile,
              'updated_date' => date('Y-m-d H:i:s')

          );
        
         $this->db->where('id', $user_id);
         $this->db->update('users', $data);
                    
         if($this->db->affected_rows()===1){
          $error['statuscode']=210;
          $error['error']='false';
          $error['message']='Profile has been updated successfully.';
           }else{

          $error['statuscode']=357;
          $error['error']='true';
          $error['message']='Error occured during update profile.';

           }
           return $error;


}




   public function isAuthorizedUserByToken($user_id,$token){

             $error = array();
             $array = array('token' => $token,'id'=>$user_id);
             $data = $this->db->get_where('users', $array)->result();
             
             if(count($data)==0)
             { 
              $error['statuscode'] = 316;
              $error['message'] = 'Unautorized user.'; 
              
             }else{
                 
                $error['statuscode'] = 200;
                

             }
             return $error;

       }

  public function isemailExist($email_id,$role){
         
       $error = array(); 

       $this->db->where('email',$email_id);
       $this->db->where('role',$role);
       $query = $this->db->get('users');
       if($query->num_rows()>0)
       {
          
          $error['statuscode']=431;
          $error['message']='Email id already exist.';
          
        }
        else{          
          $error['statuscode']=200;
        }
      return $error;
      }





/*----------check here only phone and role for update profile-- End --*/

public function isUserExistPhoneforupdate($role,$mobile,$user_id)
  {
      
              $condition = array(                         
                          'phone'=>$mobile,
                          'role'=>$role,
                          'id !='=>$user_id
                       );
              $this->db->where($condition);
              $query = $this->db->get('users');
              if($query->num_rows()>0){
                    return true;
                  }else{  
                    return false;
                  }


  }
/*----------check here only phone and role-- End --*/


/*----------------update profile code end----------*/


  /*for push notification get user check by role*/

   public function getUserPush($userid){

             $error = array();
             $array = array('id' => $userid,'role','user');
             $data = $this->db->get_where('users', $array)->result();
             

       }

  /*----For push notification end here----------*/  




 }
?>
