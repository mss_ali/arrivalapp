<?php

 class Page extends CI_Model{
   
      function __construct() 
      {

        $this->load->database();
        $this->load->library('common');

       }

       /***********Model function for Term and Condition **************/
       public function termCondition($term_cond)
       {
             $error = array();
             $array = array('identifier' => $term_cond);
             $data = $this->db->get_where('pages', $array)->result();
             $data=array_pop($data);
             if(count($data)==0)
             { 
              $error['statuscode'] = 304;
              $error['message'] = 'Invalid Identifier.';              
             }
             else
             {
               $error['statuscode'] = 201;
                $error['data'] = $data;

             }
             return $error;        
       
       }
       /*********** Term model  code end************/

        /***********Model function for about us **************/
       public function about($about)
       {
             $error = array();
             $array = array('identifier' => $about);
             $data = $this->db->get_where('pages', $array)->result();
             $data=array_pop($data);
             if(count($data)==0)
             { 
              $error['statuscode'] = 312;
              $error['message'] = 'Page does not exist.';              
             }
             else
             {
               $error['statuscode'] = 203;
                $error['data'] = $data;

             }
             return $error;        
       
       }
         /*********** About us code end************/

       

 }
?>
