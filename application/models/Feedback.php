<?php 
class Feedback extends CI_Model{
   
      function __construct() {

        $this->load->database();
        $this->load->library('common');

       }

       public function addFeedback($user_id)
       {
       	$this->load->helper('date');
       	$error=array();
       	$array=array('id'=>$user_id);
       	$user_data=$this->db->get_where('users',$array)->result();
       	// print_r($user_data);
       	// die;
       	if(count($user_data)==0)
       	{
       		$error['statuscode'] = 315;
            $error['message'] = 'User does not exist.'; 
       	}
       	else
       	{
       		  $data = array(
              'user_id' => $this->input->post('user_id'),
              'feed_content' => $this->input->post('feedback'),
              'created' => date('Y-m-d H:i:s')

          );
         $this->db->insert('feedback', $data); 
         if($this->db->affected_rows()===1)
         {
         	$error['statuscode']=204;
         	$error['error']='false';
         	$error['message']='Feedback has been saved successfully.';
         }else{
         	$error['statuscode']=357;
         	$error['error']='true';
         	$error['message']='Error occured during add feedback.';
         }
         
         }
         return $error;
       }

       public function getFeedback($user_id)
       {
       	$error=array();
       	$array=array('id'=>$user_id);
       	$user_data=$this->db->get_where('users',$array)->result();
       	if(count($user_data)==0)
       	{
       	    $error['statuscode'] = 316;
            $error['message'] = 'Unautorized user.'; 
       	}
       	else{
       		$array = array('user_id' => $user_id);
            $data = $this->db->get_where('feedback', $array)->result();
            $error['statuscode'] = 205;
            $error['message'] = 'success';
            $error['error'] = 'false';
            $error['feedback_data'] = $data;
       	}
       	return $error;

       }



}
?>
