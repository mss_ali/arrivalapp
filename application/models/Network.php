<?php 
class Network extends CI_Model
{
	function __construct()
	{
		$this->load->database();
	}
	public function getNetworks($user_id,$token)
	{
	$error = array();	
   	$array = array('id' => $user_id,'token'=>$token);
    $user_data = $this->db->get_where('users', $array)->result(); 
    
    if(!is_numeric($user_id))
      {
        $error['statuscode']=313;
        $error['message']='User id should be numeric.';
      }   
    	else if(count($user_data)==0)
   	{
   		$error['statuscode'] = 316;
        $error['message'] = 'Unauthorized user.'; 
   	}
    
   	else{
   		$this->db->select("*");
   		$this->db->from("networks");
    	$this->db->where('user_id', $user_id);
   		$network_data= $this->db->get()->result();
   	    $error['statuscode'] = 221;
        $error['network_list'] =$network_data;
        }
        return $error;
   	
	}
  public function insertNetwork(){
        $this->load->helper('date');
        $error = array();
         $data = array(
              'user_id' => $this->input->post('user_id'),
              'name' => $this->input->post('name'),
              'ssid' => $this->input->post('ssid'),
              'network_type' => $this->input->post('network_type'),
              'router_type' => $this->input->post('router_type'),
              'public_ip' => $this->input->post('publicip'),
              'network_key' => $this->input->post('netw_key'),
              'router_id' => $this->input->post('router_id'),
              'created_date' => date('Y-m-d H:i:s'),
              'updated_date' => date('Y-m-d H:i:s')

          );
         $this->db->insert('networks', $data); 
            
         if($this->db->affected_rows()===1){
          $error['statuscode']=210;
          $error['error']='false';
          $error['message']='Network has been added successfully.';
           }else{

            $error['statuscode']=357;
          $error['error']='true';
          $error['message']='Error occured during add network.';

           }
           return $error;

       }
       public function isSSISexist($user_id,$ssid){

            
           $error = array();

           $query = $this->db->get_where('networks', array('ssid' => $ssid,'user_id'=>$user_id));

           if($this->db->affected_rows()===1){
          $error['statuscode']=356;
          $error['error']='true';
          $error['message']='Network already exist please enter another SSID.';
           }else{
            $error['statuscode']=200;
          }

           return $error;


       }
       public function isRouterExist($router_id){

 
           $error = array();

           $query = $this->db->get_where('routers', array('id'=>$router_id));

         if($this->db->affected_rows()===1){
          $error['statuscode']=200;
           }else{
            
            $error['statuscode']=358;
            $error['error']='true';
            $error['message']='Wrong router ID provided.';
          }

           return $error;
           

             
      }
         /*------------listAndSearchNetwork------Start*/

                 
                  public function list_SearchNetwork(){
                 
                     $data = array();

                    if(
                          
                          !empty(
                            $this->input->post('search_param')
                            )
                      ){

                         $this->db->select('networks.*');
                         $this->db->select('users.phone');
                         $this->db->join('users','networks.user_id=users.id');
                         $this->db->like('networks.name', $this->input->post('search_param'));
                         $this->db->from('networks');
                         $this->db->where('user_id', $this->input->post('user_id'));

                       
                    }else{
                      

                          $this->db->select('networks.*');  
                          $this->db->from('networks'); 
                          $this->db->where('user_id', $this->input->post('user_id'));
                    }
                       $data = $this->db->get();
                       return $data->result();
                        
                  } 




      /*------------listAndSearchNetwork------END*/


        /*----Get network Detail ----Start here---and --isNetworkExist----------*/
      public function get($network_id = NULL){
            
           if(!isset($network_id) && empty($network_id))
           {  
           $network_id = $this->input->post('netw_id');
           $this->db->where('id',$network_id);
           $query = $this->db->get('networks');
           return $query->result();
           }else{
           $this->db->where('id',$network_id);
           $query = $this->db->get('networks');
           return $query->result();

           } 


      }
      /*----Get network Detail-----End here----------------*/

       /*----------Delete Network Start here ------Date 15 jan 2016---------------*/

   /*
          * will check the network id in network model previosly
          * first delete from network table 
          * and then find all device_id from network_device model associated with this network_id
          * and delete from device table all id come from network_device table as device_id
   */


       public function delete(){

            $error = array();
            $flag = false;
            $network_id = $this->input->post('netw_id');
            $flag = $this->deleteFromNetworkDevice($network_id);
             
             if($flag){

                    $flag = $this->deleteFromNetworks($network_id);

                     if($flag){

                             
                             $error['statuscode'] = 214;
                             $error['message'] = 'network has been deleted successfully.'; 




                     }else{

                       
                        $error['statuscode'] = 441;
                        $error['message'] = 'Something went wrong while delete network.';



                     }  
                        
             }else{
              $error['statuscode'] = 441;
              $error['message'] = 'Something went wrong while delete network.';
          }  

             return $error;  

       }

       private function deleteFromNetworkDevice($network_id){

            $this->db->where('network_id',$network_id);

            if(
               $this->db->delete('network_device')
              ){
            return true;
           }else{
            return false;
           }


       }

    

       private function deleteFromNetworks($network_id){

           $this->db->where('id',$network_id);
           if(
               $this->db->delete('networks')
              ){
            return true;
           }else{
            return false;
           }

       }





    /*-----------Delete Network End here--------date 15 Jan 2016----------------*/  


    

 /*---------Start From here for Search And List the network-----------------*/
   
   /*public function searchNetworkListNetwork(){

       
        $user_id = $this->input->post('user_id');
        $search_key = $this->input->post('search_key');
       
       if(isset($search_key) && !empty($search_key)){

        $this->db->select('*',
                          'network_device.*'
                           )
                  ->from('networks')
                  ->join('network_device','network_device.network_id=networks.id')
                  ->like('networks.name', $search_key);          
                    
                    $query = $this->db->get();   
               
                     return $query->result();

                 }else{
                          
     
                       $this->db->select('*',
                          'networks.*',
                          'network_device.*'
                           )
                  ->from('messages')
                  ->where('messages.to_user_id',$user_id)
                  ->join('networks','networks.id=messages.network_id')
                  ->join('network_device','network_device.network_id=messages.network_id');





                   $query = $this->db->get();   
               
                     return $query->result();
   


                } 
                 






   } */
   public function searchNetworkListNetwork(){

       
        $user_id = $this->input->post('user_id');
        $search_key = $this->input->post('search_key');
       
       if(isset($search_key) && !empty($search_key)){

        $this->db->select('networks.*')
                          ->select('users.phone')
                  ->from('networks')
                  ->where("network_type !=",'private')
                  ->like('networks.name', $search_key)
                  ->join('users','users.id=networks.user_id');
                    $query = $this->db->get();   
                    return $query->result();

                 }else{
                          
     
                       $this->db->select('*',
                          'networks.*',
                          'network_device.*'
                           )
                  ->from('messages')
                  ->where('messages.to_user_id',$user_id)
                  ->join('networks','networks.id=messages.network_id')
                  ->join('network_device','network_device.network_id=messages.network_id');





                   $query = $this->db->get();   
               
                     return $query->result();
   


                } 
                 






   } 
 /*---------End here for search and list the network-------------------------*/




 

 /*----------------------------Get Recent Network Ten by user----------Start------------------*/

 
   public function getRecentTenConnectedInfo(){


                 $user_id = $this->input->post('user_id');
                    
                $this->db->select('*','networks.*','network_device.*')
                         ->from('messages')
                         ->where('messages.to_user_id',$user_id)
                         ->join('networks','networks.id=messages.network_id')
                         ->join('network_device','network_device.network_id=messages.network_id')
                         ->limit(10)
                         ->order_by('messages.id','desc');
                   
                   $query = $this->db->get();   
               
                   return $query->result();

   }


 /*-----------------------------Get Recent Network Ten by user----------End ----Here----------*/





 


}

?>
