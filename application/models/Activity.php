<?php
	class Activity extends CI_Model
	{
		function __construct()
		{
			$this->load->database();
		}

		public function getActivitydetail($user_id)
		{
			$error=array();
			$mac_id=$this->input->post('mac_id');
			if(isset($mac_id) && !empty($mac_id))
			{
				
				$this->db->select('devices.*')
                ->from('devices')
                ->where('devices.mac_id',$mac_id);
                $mac_device_data=$this->db->get()->result();
                if(count($mac_device_data)==0)
                {
                	$error['statuscode'] = 430;
			        $error['message'] = 'Mac id does not exist.'; 
                }
                else
                {
                $mac_device_id=$mac_device_data[0]->id;
                $recnet_network_activity=$this->db->select('networks.*')
                ->from('networks')
                ->join('network_device', 'networks.id = network_device.network_id')
                ->where('network_device.device_id',$mac_device_id);
                $this->db->where('updated_date BETWEEN DATE_SUB(NOW(), INTERVAL 10 DAY) AND NOW()');  
                $data=$this->db->get()->result();              	
                }
			
			}
			else
			{
			$this->db->select('devices.*')
                ->from('devices')
                ->where('devices.user_id',$user_id);
            $this->db->where('updated_date BETWEEN DATE_SUB(NOW(), INTERVAL 10 DAY) AND NOW()');
            $data=$this->db->get()->result();
             }

			
			if(count($data)==0)
			   	{
			   		$error['statuscode'] = 429;
			        $error['message'] = 'Activities detail does not exist.'; 
			   	}
			   	else
			   	{		   		
					$error=$data;
			     	
		         }

			return $error;
		}


		 /*===========For Add Network API===========Start============*/
       public function is_autorizedactiveUser($userid,$token){

             $error = array();
             $array = array('id' => $userid, 'token' => $token,'role'=>'user');
             $data = $this->db->get_where('users', $array)->result();
             
             if(count($data)==0)
             { 
              $error['statuscode'] = 316;
              $error['message'] = 'Unautorized user.'; 
              
             }else{
                 $error['statuscode'] = 200;
                

             }
             return $error;

       }
       /*===========Form Add Netwrk API===========End==============*/

        public function macidExist($mac_id){

         /*check here if user provide valid id of device table ID*/

         $error = array();  
       $this->db->where('mac_id',$mac_id);
       $query = $this->db->get('devices');
        if($query->num_rows()==0){
          
          $error['statuscode']=424;
          $error['message']='Wrong mac ID.';
          
        }else{
          
          $error['statuscode']=200;
        }
      return $error;




     }

   }





?>