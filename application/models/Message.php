<?php
	class Message extends CI_Model
	{
		function __construct()
		{
			$this->load->database();
		}

		public function getAlertdetail()
		{
			$error=array();
			$this->db->select('*');
			$this->db->where('created_date BETWEEN DATE_SUB(NOW(), INTERVAL 2 DAY) AND NOW()');
			$this->db->from('messages');
			$this->db->order_by('created_date','desc');
			$this->db->limit('10');
			$alert_data=$this->db->get()->result();
			if(count($alert_data)==0)
			   	{
			   		$error['statuscode'] = 429;
			        $error['message'] = 'No recent history found.'; 
			   	}
			   	else
			   	{
					$error=$alert_data;
		         }

			return $error;
		}

			public function getHistory($limit,$start){

               $this->db->order_by('created_date','desc');
               $this->db->limit($limit,$start);
               $query = $this->db->get('messages')->result_array();
               return $query; 
               
        
		}

		public function getAllRecord(){


			$total_record = $this->db->count_all('messages');
			return $total_record;
		}

		/*---START---------Delete history------------------------------------------------*/		

		public function isMessageExist($m_id){

               $error = array();  
               $this->db->where('id',$m_id);
               $query = $this->db->get('messages');
               if($query->num_rows()==0){  
                    $error['statuscode'] = 449;
                    $error['messages'] = 'This message does not exist.';
			   }else{
			   	$error['statuscode'] = 200;
			   }
			   return $error;
 
		}

		public function eraseMessage($m_id){

             $error = array();
			 $this->db->where('id',$m_id);
			 $query = $this->db->delete('messages');
			 if($query==1){
                    
                     $error['statuscode'] = 222;
                     $error['messages'] = 'History has been deleted successfully.';
			 }


          return $error;
		}


/*---END---------Delete history------------------------------------------------*/	



/*----insert message after push notification ---START-----------*/

/*
     Either connect OR disconnect

*/

            
             public function insertMessage(){
                
              $this->load->helper('date');
              $error = array();	
              $data = array(	
			             	'to_user_id' => $this->input->post('to_user_id'),
			             	
			             	'from_user_id' => $this->input->post('from_user_id'),
			                
			                'network_id' => $this->input->post('netw_id'),
			                
			                'status' => $this->input->post('status'),
			                
			                'device_id' => $this->input->post('device_id'),
			                
			                'to_user_id' => $this->input->post('to_user_id'),
			                
			                'created_date' => date('Y-m-d H:i:s'),
			                'msg_content' => 'dummy message content'

                );

                    $this->db->insert('messages', $data); 
            
					         if($this->db->affected_rows()==1){
					          $error['statuscode']=200;
					          $error['error']='false';
					          $error['message']='Network has been added successfully.';
					           }else{

					          $error['statuscode']=357;
					          $error['error']='true';
					          $error['message']='Error occured during add network.';

					           }
					          
					           return $error;
					 


             }

             public function getInfoStatus($user_id,$network_id){

                    /*here user, not admin only user*/

                    $response = array();
                    $this->db->select('users.name');
                    $this->db->select('users.email');
                    $this->db->select('users.phone');
                    $this->db->where('id',$user_id);
                    $user_detail = $this->db->get('users')->result();
                    
                    $this->db->select('networks.name AS networkName');
                    $this->db->where('id',$network_id);
                    $network_detail = $this->db->get('networks')->result();
                    
                    $response['user_detail'] = $user_detail[0];
                    $response['network_detail'] = $network_detail[0];
                    $response['statuscode']=200;
                    return $response;
                    



             }





/*----insert message after push notification ---END-----------*/	



   }





?>