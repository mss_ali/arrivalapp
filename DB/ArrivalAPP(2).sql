-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 01, 2016 at 10:49 AM
-- Server version: 5.5.40-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ArrivalAPP`
--

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE IF NOT EXISTS `devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `device_name` varchar(50) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `mac_id` varchar(20) NOT NULL,
  `phone_id` text,
  `email` varchar(100) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `devices_fk0` (`user_id`),
  KEY `mac_id` (`mac_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=184 ;

--
-- Dumping data for table `devices`
--

INSERT INTO `devices` (`id`, `user_id`, `admin_id`, `device_name`, `mobile`, `mac_id`, `phone_id`, `email`, `created_date`, `updated_date`) VALUES
(182, 290, 289, 'Samsung', '1545188180', '64:B3:10:9E:DE:4F', '', 'deee@g.com', '2016-03-31 10:17:39', '0000-00-00 00:00:00'),
(183, 293, 289, 's2', '5404611648', '64:B3:10:9E:DE:4F', '', 'd@g.com', '2016-03-31 18:33:45', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `feed_content` text NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `to_user_id` int(11) NOT NULL,
  `network_id` int(11) NOT NULL,
  `device_id` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `from_user_id` int(11) NOT NULL,
  `msg_content` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `messages_fk0` (`to_user_id`),
  KEY `messages_fk1` (`from_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2593 ;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `to_user_id`, `network_id`, `device_id`, `status`, `created_date`, `from_user_id`, `msg_content`) VALUES
(2591, 290, 422, 'APA91bGPvu94A9XAXujdBDrqKappbNMlq5WrTp-cjg8MgKraq2nHEvX0uZ5kD-4k9T6MldD1kcaTMe7K0T8pXXrFacYqlHlvmqYIhlVNmlH2RNSqsbXlutcTA-rrBykuBxegviN4f_9k', 'connected', '2016-03-31 10:17:39', 289, 'dummy message content'),
(2592, 293, 422, 'APA91bE3rqQ2-1kHT-JtHz8gaKn7QYy-qs5SWl_-tObkYXGcGKKQ1HZUfJ6lkBneCtZPFGaaO3x4bVG2kndzvw9Ov9xoS1un_Vn7sHwCIAoazZc3sYWYmlTm403Q8qSe7hW_8RQ518PW', 'connected', '2016-03-31 18:33:45', 289, 'dummy message content');

-- --------------------------------------------------------

--
-- Table structure for table `networks`
--

CREATE TABLE IF NOT EXISTS `networks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT 'user_id is id of user role admin',
  `name` varchar(50) NOT NULL,
  `ssid` varchar(100) NOT NULL,
  `network_type` varchar(10) NOT NULL,
  `router_id` int(11) NOT NULL,
  `router_type` varchar(40) NOT NULL,
  `public_ip` varchar(30) NOT NULL,
  `network_key` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `networks_fk0` (`user_id`),
  KEY `networks_fk1` (`router_id`),
  KEY `user_id` (`user_id`),
  KEY `router_id` (`router_id`),
  KEY `user_id_2` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=424 ;

--
-- Dumping data for table `networks`
--

INSERT INTO `networks` (`id`, `user_id`, `name`, `ssid`, `network_type`, `router_id`, `router_type`, `public_ip`, `network_key`, `created_date`, `updated_date`) VALUES
(386, 260, 'the crib', 'ATT304', 'public', 12, 'Public', '192.168.1.254', '8105286826', '2016-02-28 00:45:43', '2016-02-28 00:45:43'),
(417, 280, 'MSS-WF1', 'MSS-WF1', 'public', 11, 'Private', '', 'MasterWF1#', '2016-03-07 12:19:00', '2016-03-07 12:19:00'),
(418, 278, 'MSS-WF2', 'MSS-WF2', 'public', 12, 'Public', '', 'MasterWF2#', '2016-03-07 15:11:22', '2016-03-07 15:11:22'),
(419, 285, 'shivani', 'dvd', 'public', 11, 'Private', '', '11', '2016-03-09 16:12:33', '2016-03-09 16:12:33'),
(420, 286, 'test network 1', 'arrivalmobileapp', 'public', 11, 'Private', '', 'SBG6580C58373', '2016-03-29 10:39:08', '2016-03-29 10:39:08'),
(421, 288, 'vgsgg', 'hshsvs', 'public', 11, 'Private', '', 'admin', '2016-03-31 09:45:12', '2016-03-31 09:45:12'),
(422, 289, 'network1', 'aa', 'public', 12, 'Public', '', 'abc', '2016-03-31 10:12:04', '2016-03-31 10:12:04'),
(423, 289, 'network2', 'dd', 'public', 12, 'Public', '', '2', '2016-03-31 10:47:34', '2016-03-31 10:47:34');

-- --------------------------------------------------------

--
-- Table structure for table `network_device`
--

CREATE TABLE IF NOT EXISTS `network_device` (
  `device_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `network_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `network_device_fk1` (`device_id`),
  KEY `network_device_fk0` (`network_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=599 ;

--
-- Dumping data for table `network_device`
--

INSERT INTO `network_device` (`device_id`, `id`, `network_id`) VALUES
(182, 597, 422),
(183, 598, 422);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(20) NOT NULL,
  `content` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `identifier` (`identifier`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `identifier`, `content`) VALUES
(9, 'term_cond', 'DUMMY TEXT   \nA Terms of Service Agreement is a set of regulations that users must agree to follow in order to use a service. Terms of Use is often named Terms of Service and sometimes Terms and Conditions, as well as Disclaimer, mostly when addressing website usage.\n\nA Terms of Service Agreement is a set of regulations that users must agree to follow in order to use a service. Terms of Use is often named Terms of Service and sometimes Terms and Conditions, as well as Disclaimer, mostly when addressing website usage.\n\nA Terms of Service Agreement is a set of regulations that users must agree to follow in order to use a service. Terms of Use is often named Terms of Service and sometimes Terms and Conditions, as well as Disclaimer, mostly when addressing website usage.\n\nA Terms of Service Agreement is a set of regulations that users must agree to follow in order to use a service. Terms of Use is often named Terms of Service and sometimes Terms and Conditions, as well as Disclaimer, mostly when addressing website usage.'),
(10, 'about_us', 'DUMMY TEXT\nAbout Text Matters\nText Matters is a UK-based information design consultancy, established in Reading in 1990 by partners Mark Barratt and Sue Walker.\nWe use our experience in language, design, systems and process improvement to deal with communication issues in a way that is focused on users. We work in print and electronic media, and specialise in explaining things through:\nclear language\ntypography and graphic design\nresearch and copywriting\nprocess analysis.\nText Matters is a UK-based information design consultancy, established in Reading in 1990 by partners Mark Barratt and Sue Walker.\nWe use our experience in language, design, systems and process improvement to deal with communication issues in a way that is focused on users. We work in print and electronic media, and specialise in explaining things through:\nclear language\ntypography and graphic design\nresearch and copywriting\nprocess analysis.\n\nText Matters is a UK-based information design consultancy, established in Reading in 1990 by partners Mark Barratt and Sue Walker.\nWe use our experience in language, design, systems and process improvement to deal with communication issues in a way that is focused on users. We work in print and electronic media, and specialise in explaining things through:\nclear language\ntypography and graphic design\nresearch and copywriting\nprocess analysis.');

-- --------------------------------------------------------

--
-- Table structure for table `routers`
--

CREATE TABLE IF NOT EXISTS `routers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `routers`
--

INSERT INTO `routers` (`id`, `name`, `type`) VALUES
(11, 'Office', 'Private'),
(12, 'Home', 'Public');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` enum('admin','user') NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `term_cond` tinyint(1) NOT NULL,
  `password` varchar(100) NOT NULL,
  `token` varchar(256) NOT NULL,
  `device_type` varchar(20) NOT NULL,
  `country_code` varchar(5) DEFAULT NULL,
  `phone` varchar(15) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `term_cond` (`term_cond`),
  KEY `role` (`role`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=294 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role`, `name`, `email`, `device_id`, `term_cond`, `password`, `token`, `device_type`, `country_code`, `phone`, `created_date`, `updated_date`) VALUES
(286, 'admin', 'damian', 'compedge123@yahoo.com', 'APA91bExa39CFhcu_uK2H1eVLmfCjbfb4ocLQReLbYyG4_Fbsuwy7TxlG97KiyKozxfiedcblr1DS7L7YuimiiiNdr3g86w5PW5n_dtsqxOCTk0AurExLgyBgC3DA3LBIFhWRSQF48-q', 1, 'e23a6185d9e40b9a889fc2d315e424ab', '6443093a17e96cabb469f29f02c97d2e', 'android', '+1', '4694075276', '2016-03-29 10:21:04', '2016-03-31 17:12:32'),
(287, 'user', 'test phone', 'damianhenderson9@yahoo.com', 'APA91bF9dK7YcxsDEVrgHmnSNKoFiWnkMc9qHP7KA0FGaZ_h_QNTaR1Vqc7D6-nCaBo_1cJaoJ4sLDWTTnkzEhoUNFF2nOiKlQE7OWCtApv-_bdJzZ7EpmxCM344Th655Lc-MrQ5JjCp', 1, '78d1860ed582f28f7f2b6d77cb3345df', '50b8b4b401d53f4750d438e95d010355', 'android', '+1', '9999999999', '2016-03-29 10:43:10', '2016-03-29 05:13:10'),
(288, 'admin', 'deepak', 'dd@g.com', 'APA91bHXpMMzj-cqB508WUVbhneotG8RjsLYuvdkELyLMrfpdgJIsmeJDeqDyNl8JuJsny26T_RZf8mLARVxOU8_3HQvPyHmKYmX27P9uDDZI3KFuOfqxl598pjxxEwxTweWE3VOV7Uw', 1, '61cc0e405f4b518d264c089ac8b642ef', 'a6ab7efbd0231274c45f8abfa2d59240', 'android', '+376', '1245867646', '2016-03-31 09:43:42', '2016-03-31 04:13:42'),
(289, 'admin', 'dee', 'de@g.com', 'APA91bEpRMN8MxUm2GnDLiZrUm1fIwxhf-EuQwssJRDP1siSMCGGIEvoWvl1qyi7pTXEb5c-iknZPrd6eK4grLfiEyxm04R6MGo1zl0rphstX6idiuiG8AWvZJ0kXVIiZiz1TijPTb2t', 1, '61cc0e405f4b518d264c089ac8b642ef', '7da0b64baa0f98c96b907852a9115256', 'android', '+244', '1234567890', '2016-03-31 09:46:43', '2016-03-31 13:01:13'),
(290, 'user', 'deep', 'deee@g.com', '1', 1, '61cc0e405f4b518d264c089ac8b642ef', '92aa89d7585ce793c6e98295683eed6f', 'android', '+376', '1545188180', '2016-03-31 09:48:44', '2016-03-31 12:05:24'),
(291, 'admin', 'sparatak', 'spartak@g.com', 'APA91bFbLgESNUbYK8r33yTJLkMqY69JzThHJJ2URV_WhQCEntxYwRvwZZuCvnale4yUK2fpRLuQljU0sobn-h-o5kT-9IQ0aG1t_HTrFn7woM9K3QzQ5sHZyhkwql9pvqD7SFwB_P7h', 1, '9f6e6800cfae7749eb6c486619254b9c', 'da240c1e729cf0b2ef5397b3b44e69ae', 'android', '+91', '9041778851', '2016-03-31 13:13:41', '2016-03-31 13:07:02'),
(292, 'user', 'jdh', 'dee@g.com', '', 1, '61cc0e405f4b518d264c089ac8b642ef', '', 'android', '+213', '8464618634', '2016-03-31 13:18:42', '2016-03-31 13:00:19'),
(293, 'user', 'dee', 'd@g.com', 'APA91bE3rqQ2-1kHT-JtHz8gaKn7QYy-qs5SWl_-tObkYXGcGKKQ1HZUfJ6lkBneCtZPFGaaO3x4bVG2kndzvw9Ov9xoS1un_Vn7sHwCIAoazZc3sYWYmlTm403Q8qSe7hW_8RQ518PW', 1, '187ef4436122d1cc2f40dc2b92f0eba0', '02d28b3f66ee8c66e9508ca7e8c62e55', 'android', '+213', '5404611648', '2016-03-31 18:32:41', '2016-03-31 13:02:41');

-- --------------------------------------------------------

--
-- Table structure for table `user_macs`
--

CREATE TABLE IF NOT EXISTS `user_macs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `mac_id` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=329 ;

--
-- Dumping data for table `user_macs`
--

INSERT INTO `user_macs` (`id`, `user_id`, `mac_id`) VALUES
(317, 286, 'E8:50:8B:4F:CD:F0'),
(318, 287, '34:23:BA:80:1C:65'),
(319, 288, '34:AA:8B:EE:CA:E5'),
(320, 289, '64:B3:10:9E:DE:4F'),
(321, 290, '64:B3:10:9E:DE:4F'),
(322, 290, 'e8:91:20:f1:f6:eb'),
(323, 291, '90:21:81:36:37:c1'),
(324, 292, '64:B3:10:9E:DE:4F'),
(325, 290, '11-11-11-11-22-25'),
(326, 289, '11-11-11-11-22-25'),
(327, 289, '90:21:81:36:37:c1'),
(328, 293, '64:B3:10:9E:DE:4F');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
