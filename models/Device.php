<?php
class Device extends CI_Model{
  	function __construct(){
  		$this->load->database();
  	}

   
      public function isMacIdExist($macid,$network_id){
         
       $error = array();  
       $this->db->where('mac_id',$macid);
       $query = $this->db->get('devices');
        if($query->num_rows()==0){
          $error['statuscode']=200;
        }else{

           $isDeviceAlreadyConnected = $this->isdeviceAlreadyconnected($macid,$network_id);
           $error = $isDeviceAlreadyConnected;

           if($error['statuscode']===200){
           	  $error['statuscode'] = 200;
              $error['mac_exist_not_this_network'] = 200;
           }           

          /*$error['statuscode']=327;
          $error['message']='Mac id is already exist.';*/



        }
      return $error;
      }


  protected function isdeviceAlreadyconnected($macid,$network_id){
           
            $error = array();
            $this->db->select('id');  
            $this->db->where('mac_id',$macid);
            $query = $this->db->get('devices')->result();
            $device_id = $query[0]->id;

            
            
            $this->db->where('device_id',$device_id);
            $this->db->where('network_id',$network_id);
            $network_device = $this->db->get('network_device');
            //print_r($network_device);die;
            if($network_device->num_rows()!=0){
            	$error['statuscode']=331;
            	$error['message']='This device already connected to this network';

            }else{
            	$error['statuscode']=200;
            	
            }
           return $error; 

  }




      public function isNetworkIdExist($network_id){
         
       $error = array();  
       $this->db->where('id',$network_id);
       $query = $this->db->get('networks');
        if($query->num_rows()==0){
          
          $error['statuscode']=329;
          $error['message']='Network id does not recognize.';
          
        }else{
          
          $error['statuscode']=200;
        }
      return $error;
      }

    public function insertDevice($data){
        $this->load->helper('date');
        $error = array();
        $this->db->select('mac_id');  
        $this->db->where('id',$data['user_mac_id']);
        $query = $this->db->get('user_macs')->result();
        $mac_id = $query[0]->mac_id;
        /*Check mac id in device table*/
        $this->db->select('id');  
        //$this->db->where('mac_id',$mac_id);
        $this->db->where(array('mac_id'=>$mac_id,'admin_id'=>$data['admin_id'],'user_id'=>$data['user_id']));
        $query = $this->db->get('devices')->result();

        $device_id = "";
        if(isset($query[0]->id)){
          $device_id = $query[0]->id;
        }
        if($device_id){
            $netData = array('device_id' => $device_id,'network_id' => $data['network_id']);
          /*Check already connected device*/
            $this->db->select('id');  
            $this->db->where($netData);
            $netDevices = $this->db->get('network_device')->result();
            if(count($netDevices)>0){
              $error['statuscode']=425;
              $error['message']='This Network id already connected with this device.Please select another Network id.'; 
            }else{
              $this->db->insert('network_device', $netData);
              if($this->db->affected_rows()===1){
                $error['statuscode']=215;
                $error['error']='false';
                $error['message']='Device has been added successfully.';
              }else{
                  $error['statuscode']=330;
                  $error['error']='true';
                  $error['message']='Error occured during adding device.';

              }
            }
        }else{
             $this->db->where(array('admin_id'=>$data['admin_id'],'user_id'=>$data['user_id']));
             $query = $this->db->get('devices')->result();
             $device_id = "";
             if(isset($query[0]->id)){
                 $device_id = $query[0]->id;
		 $devData = array(
                  'user_id' => $data['user_id'],
                  'admin_id' => $data['admin_id'],
                  'device_name' => $data['device_name'],
                  'mobile' => $data['mobile_no'],
                  'mac_id' => $mac_id,
                  'phone_id' => $data['phone_id'],
                  'email' => $data['email'],
                  'created_date' => date('Y-m-d H:i:s')
                 );
		$this->db->where('id',$device_id);
		$this->db->update('devices',$devData);
             }else{
               $devData = array(
                  'user_id' => $data['user_id'],
                  'admin_id' => $data['admin_id'],
                  'device_name' => $data['device_name'],
                  'mobile' => $data['mobile_no'],
                  'mac_id' => $mac_id,
                  'phone_id' => $data['phone_id'],
                  'email' => $data['email'],
                  'created_date' => date('Y-m-d H:i:s')
              );
              $this->db->insert('devices', $devData);
	     }
            
            
            if($this->db->affected_rows()===1){
               if($device_id){
                     $netData = array('device_id' => $device_id,'network_id' => $data['network_id']);
                     $this->db->select('id');  
            	     $this->db->where($netData);
                     $netDevices = $this->db->get('network_device')->result();
                     if(count($netDevices)>0){
		           $error['statuscode']=425;
			   $error['message']='This Network id already connected with this device.Please select another Network id.'; 
		     }else{
                        $insert_id = $device_id;
		        $netData = array('device_id' => $insert_id,'network_id' => $data['network_id']);
		        $this->db->insert('network_device', $netData);   
		        if($this->db->affected_rows()===1){
		          $error['statuscode']=215;
		          $error['error']='false';
		          $error['message']='Device has been added successfully.';
		        }else{
		          $error['statuscode']=330;
		          $error['error']='true';
		          $error['message']='Error occured during adding device.';
		        }
                     }
                }else{
                        $insert_id = $this->db->insert_id();
		        $netData = array('device_id' => $insert_id,'network_id' => $data['network_id']);
		        $this->db->insert('network_device', $netData);   
		        if($this->db->affected_rows()===1){
		          $error['statuscode']=215;
		          $error['error']='false';
		          $error['message']='Device has been added successfully.';
		        }else{
		          $error['statuscode']=330;
		          $error['error']='true';
		          $error['message']='Error occured during adding device.';
		        } 
		}
            }else{
              $error['statuscode']=330;
              $error['error']='true';
              $error['message']='Error occured during adding device.';
            }
        }
      return $error;
    }

       /*------Get conneted device information Strat Here date 13 Jan 2016----*/

    public function getDevice($data){
      $devices = array();
      $admin_id = $data['admin_id'];
      $search=$data['search'];
      $netw_id = $data['netw_id'];
      if($search){
        if($netw_id){
          $this->db->select('devices.*')->from('devices')->join('network_device','devices.id = network_device.device_id')->where(array('devices.admin_id'=>$admin_id,'devices.device_name LIKE'=>"%$search%",'network_device.network_id'=>$netw_id));
        }else{
          $this->db->select('devices.*')->from('devices')->where(array('devices.admin_id'=>$admin_id,'devices.device_name LIKE'=>"%$search%"));
        }
      }else{
        if($netw_id){
          $this->db->select('devices.*')->from('devices')->join('network_device','devices.id = network_device.device_id')->where(array('devices.admin_id'=>$admin_id,'network_device.network_id'=>$netw_id));
        }else{
          $this->db->select('devices.*')->from('devices')->where('devices.admin_id',$admin_id);
        }
      }
      $devices = $this->db->get()->result();
      return $devices;
    } 
      /*-------Get Connected device information End here---------------------*/


         /*---update device ----Start---------here---------------*/
    public function isOldNetworkIdExist($network_id,$device_id){
      $error = array();
      $data = array('network_id'=>$network_id,'device_id'=>$device_id);  
      $this->db->where($data);
      $query = $this->db->get('network_device');
      if($query->num_rows()==0){
          $error['statuscode']=423;
          $error['message']='Wrong old network id.';
        }else{
          $error['statuscode']=200;
        }
      return $error;
    }

    public function IdExist($id){
        $error = array();  
        $this->db->where('id',$id);
        $query = $this->db->get('devices');
        if($query->num_rows()==0){
          $error['statuscode']=424;
          $error['message']='Wrong device ID.';
        }else{
          $error['statuscode']=200;
        }
      return $error;
    }

    public function isAuthorizeNetworkOwner($network_id,$user_id){
      $error = array();
      $data = array('id'=>$network_id,'user_id'=>$user_id);
      $this->db->where($data);
      $query = $this->db->get('networks');
      if($query->num_rows()==0){
        $error['statuscode']=425;
        $error['message']='User not Authorize to Access this network.';
      }else{
        $error['statuscode']=200;
      }
      return $error;
    }

     public function checkMac_idExist($device_id,$user_id,$mac_id){

      


           $error = array();
              $data = array(
                  'id !='=>$device_id,
                  //'user_id !='=>$user_id,
                  'mac_id'=>$mac_id

                );  
                 $this->db->where($data);
                 $query = $this->db->get('devices');

                  if($query->num_rows()!=0){
                    
                    $error['statuscode']=426;
                    $error['message']='This mac id already exist.please provide another mac id.';
                    
                  }else{
                    
                    $error['statuscode']=200;
                  }
                return $error;








     }


  public function updateDevice($data){
    $error = array();
    $update_data = array(
      'device_name' => $data['device_name'],
      'mobile' => $data['mobile_no'],
      'phone_id' => $data['phone_id'],
      'email' => $data['email']
    );
    // echo "<pre>";print_r($data['id']);die;
    $this->db->where('id', $data['id']);
    $this->db->update('devices', $update_data);
    if($data['new_network_id']!=$data['old_network_id']){
       $condition = array('device_id'=>$data['id'],'network_id'=>$data['old_network_id']);
       $condition1 = array('device_id'=>$data['id'],'network_id'=>$data['new_network_id']);
       $this->db->where($condition);
       $query = $this->db->get('network_device')->result();
       $netDevId=$query[0]->id;

       $this->db->where($condition1);
       $query1 = $this->db->get('network_device')->result();
       if(isset($query1[0]->id)){
        $this->db->where(array('id'=>$query1[0]->id));
        $this->db->delete('network_device');
       }

       $updateNet=array('network_id'=>$data['new_network_id']);
       $this->db->where('id', $netDevId);
       $this->db->update('network_device', $updateNet);
       if($this->db->affected_rows()){
          $error['statuscode']=216;
          $error['message']='Device has been updated successfully.';
       }else{
           $error['statuscode']=427;
           $error['message']='Something went wrong while updating the device.';
       }
    }else{
        $error['statuscode']=216;
        $error['message']='Device has been updated successfully.';
    } 
    return $error;
  }
 /*----update device ----end-----------here--------------*/


/*------------device and network exist code----------------*/
public function removenetDevice($device_id,$network_id){
$error=array();
$flag=false;
$flag = $this->deleteFromnetdevice($device_id,$network_id);
                  if($flag){                             
                             $error['statuscode'] = 217;
                             $error['message'] = 'Device has been remove successfully.'; 

                     }else{
                  
                        $error['statuscode'] = 441;
                        $error['message'] = 'Something went wrong while delete network.';
                     }                           

             return $error; 
}
private function deleteFromnetdevice($device_id,$network_id){
   $this->db->where('device_id',$device_id);
   $this->db->where('network_id',$network_id);
   if(
       $this->db->delete('network_device')
      ){
    return true;
   }else{
    return false;
   }
}

    public function isNetanddeviceIdExist($device_id,$network_id){
         
       $error = array();  
       $this->db->where('network_id',$network_id);
       $this->db->where('device_id',$device_id);
       $query = $this->db->get('network_device');
        if($query->num_rows()==0){
          
          $error['statuscode']=431;
          $error['message']='Network id and device id does not recognize.';
          
        }else{
          
          $error['statuscode']=200;
        }
      return $error;
      }


public function isAdminAccessOwnNet($user_id,$network_id){
    $error = array();
      $query = $this->db->get_where('networks', array('id' => $network_id,'user_id' => $user_id));
       if($query->num_rows()===0){
        $error['statuscode']=334;
        $error['message'] = 'This network not associated with this user.';
       }else{
          $error['statuscode']=200;        
       }
        
       return $error;    

   } 

public function isAdminAccessOwnDevice($user_id,$device_id){
    $error = array();
      $query = $this->db->get_where('devices', array('id' => $device_id,'admin_id' => $user_id));
       if($query->num_rows()===0){
        $error['statuscode']=432;
        $error['message'] = 'This device not associated with this user.';
       }else{
          $error['statuscode']=200;        
       }
       return $error;
   } 

/*------------------device and network exist code end------------*/




/*---For edit network-------------*/
    public function isAuthorizeNetworkOwnerEdit($network_id,$user_id){


              $error = array();
              $data = array(
                  'id'=>$network_id,
                  'user_id'=>$user_id

                );  
                 $this->db->where($data);
                 $query = $this->db->get('networks');
                  if($query->num_rows()==0){
                    
                    $error['statuscode']=425;
                    $error['message']='User not Authorize to Access this network.';
                    
                  }else{
                       $error['statuscode']=200;
                  }
                  return $error;
                }
/*For Edit network------------------*/


/*----------------mac id exist in user----------------*/
    public function  isMacIdExistinuser($user_mac_id, $user_id){
            $error = array();  
            $this->db->select('*');  
            $this->db->where('id',$user_mac_id);
            $query = $this->db->get('user_macs');
            if($query->num_rows()==0){
              $error['statuscode']=331;
              $error['message']='User mac id does not exist.';
            }else{
              if($query->result()[0]->user_id!=$user_id){
                $error['statuscode']=531;
                $error['message']='User mac id is not valid.';
              }else{
                $error['statuscode']=200;
              }
            } 
          return $error;
      }

     /*----------------mac id exist in user code end----------------*/



  }
?>
