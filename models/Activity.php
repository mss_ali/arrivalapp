<?php
	class Activity extends CI_Model
	{
		function __construct()
		{
			$this->load->database();
		}

		public function getActivitydetail($admin_id){
			$error=array();	
			$this->db->select('devices.*')->from('devices')->where('devices.admin_id',$admin_id);
      $device_data=$this->db->get()->result();
      if(count($device_data)==0){
        //$error['statuscode'] = 430;
       // $error['message'] = 'No device added yet.'; 
      }else{
        $date = date('Y-m-d');
        $days_ago = date('Y-m-d', strtotime('-10 days', strtotime($date)));
        $recentDevices = $this->db->select('*')->from('devices')->join('network_device','devices.id=network_device.device_id')->join('networks','network_device.network_id=networks.id')->where(array('devices.created_date >=' => $days_ago,'devices.admin_id'=> $admin_id)); 
        $data=$this->db->get()->result(); 
        if(!empty($data)){
          $error=$data;  
        }else{
         // $error['statuscode'] = 429;
          //$error['message'] = 'No Recent Activity found.';             
        }
      }
			return $error;
		}


		 /*===========For Add Network API===========Start============*/
       public function is_autorizedactiveUser($userid,$token){

             $error = array();
             $array = array('id' => $userid, 'token' => $token,'role'=>'user');
             $data = $this->db->get_where('users', $array)->result();
             
             if(count($data)==0)
             { 
              $error['statuscode'] = 316;
              $error['message'] = 'Unautorized user.'; 
              
             }else{
                 $error['statuscode'] = 200;
                

             }
             return $error;

       }
       /*===========Form Add Netwrk API===========End==============*/

        public function macidExist($mac_id){

         /*check here if user provide valid id of device table ID*/

         $error = array();  
       $this->db->where('mac_id',$mac_id);
       $query = $this->db->get('devices');
        if($query->num_rows()==0){
          
          $error['statuscode']=424;
          $error['message']='Wrong mac ID.';
          
        }else{
          
          $error['statuscode']=200;
        }
      return $error;




     }

   }





?>
