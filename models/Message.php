<?php
	class Message extends CI_Model
	{
		function __construct()
		{
			$this->load->database();
		}

public function getAlertdetail($data){
	$error=array();
	$admin_id=$data['admin_id'];
	$date = date('Y-m-d');
    $days_ago = date('Y-m-d', strtotime('-2 days', strtotime($date)));
	$this->db->select('*')->from('messages')->where(array('messages.created_date >='=>$days_ago,'messages.to_user_id'=>$admin_id))->order_by('created_date','desc')->limit('10');
	$alert_data=$this->db->get()->result();
	if(count($alert_data)==0){
	   		$error['statuscode'] = 429;
	        $error['message'] = 'No recent alert found.'; 
	}else{
			$error=$alert_data;
    }
	return $error;
}

public function getHistory($limit,$start,$user_id){
$newt_id=$this->input->post('netw_id');
$day=$this->input->post('day');
$month=$this->input->post('month');
$year=$this->input->post('year');

  $cond=array();
      if( isset($newt_id) && !empty($newt_id))
        
      {
       
  $this->db->where(array('messages.network_id'=>$newt_id));

      }
    if(isset($day) && !empty($day))
      {
         $this->db->where(array('DATE(created_date)'=>$day));
        
      }
      if(isset($month) && !empty($month))
      {
        $this->db->where(array('MONTH(created_date)'=>$month));
      }
      if(isset($year) && !empty($year))
      {
      $this->db->where(array('Year(created_date)'=>$year));
      }
     
       $this->db->where(array('messages.to_user_id'=>$user_id));
       $this->db->order_by('created_date','desc');
       $this->db->limit($limit,$start);
       $query = $this->db->get('messages')->result_array();
        
        if(count($query)==0){
                $error=$query;
        }else{
             
             foreach($query as $key=>$value){
                $this->db->select('device_name')->where(array('devices.admin_id'=>$user_id,'devices.user_id'=>@$value['from_user_id']));
	        $devices = $this->db->get('devices')->result_array();
                
                $devices= max($devices);
                $query[$key]['device_name']=@$devices['device_name'];

             }	
            //echo "<pre>";print_r($query);die;
              $error=$query;
        }
        return $error;

       

}

public function getAllRecord($user_id){
    $this->db->from('messages'); 
    $this->db->where('messages.to_user_id',$user_id);
	  $total_record = $this->db->count_all_results();
	  return $total_record;
}

/*---START---------Delete history------------------------------------------------*/		

		public function isMessageExist($m_id){
           $error = array();  
           $this->db->where('id',$m_id);
           $query = $this->db->get('messages');
           if($query->num_rows()==0){  
                $error['statuscode'] = 449;
                $error['messages'] = 'This message does not exist.';
		   }else{
		   	$error['statuscode'] = 200;
		   }
		   return $error;
		}

		public function eraseMessage($m_id){

             $error = array();
			 $this->db->where('id',$m_id);
			 $query = $this->db->delete('messages');
			 if($query==1){
                    
                     $error['statuscode'] = 222;
                     $error['messages'] = 'History has been deleted successfully.';
			 }


          return $error;
		}


/*---END---------Delete history------------------------------------------------*/	



/*----insert message after push notification ---START-----------*/

/*
     Either connect OR disconnect

*/

            
public function insertMessage($data){
  $this->load->helper('date');
  $error = array();	
  $data = array(	
             	'to_user_id' => $data['to_user_id'],
             	
             	'from_user_id' => $data['from_user_id'],
                
                'network_id' => $data['netw_id'],
                
                'status' => $data['status'],
                
                'device_id' => $data['device_id'],
                
                'to_user_id' => $data['to_user_id'],
                
                'created_date' => date('Y-m-d H:i:s'),
                'msg_content' => 'dummy message content'

    );
  $this->db->insert('messages', $data); 
  if($this->db->affected_rows()==1){
	  $error['statuscode']=200;
	  $error['error']='false';
	  $error['message']='Network has been added successfully.';
   }else{
	  $error['statuscode']=357;
	  $error['error']='true';
	  $error['message']='Error occured during add network.';
   }
   return $error;
}

             

             public function getInfoStatus($user_id,$network_id){

                    /*here user, not admin only user*/

                    $response = array();
                    $this->db->select('users.name');
                    $this->db->select('users.email');
                    $this->db->select('users.phone');
                    $this->db->where('id',$user_id);
                    $user_detail = $this->db->get('users')->result();
                    
                    $this->db->select('networks.name AS networkName');
                    $this->db->where('id',$network_id);
                    $network_detail = $this->db->get('networks')->result();
                    
                    $response['user_detail'] = $user_detail[0];
                    $response['network_detail'] = $network_detail[0];
                    $response['statuscode']=200;
                    return $response;
                    



             }





/*----insert message after push notification ---END-----------*/	




public function ConsaveMessages($data){
 //$this->load->helper('date');
  $error = array(); 
  $data = array(  
              'to_user_id' => $data['to_user_id'],
              
              'from_user_id' => $data['from_user_id'],
                
                'network_id' => $data['network_id'],
                
                'status' => $data['status'],
                
                'device_id' => $data['device_id'],
                
                'to_user_id' => $data['to_user_id'],
                
                'created_date' => date('Y-m-d H:i:s'),
                

    );
  $this->db->insert('messages', $data); 
  if($this->db->affected_rows()==1){
    $error['statuscode']=200;
    $error['error']='false';
    $error['message']='Message Successfully Done';
   }else{
    $error['statuscode']=901;
    $error['error']='true';
    $error['message']='Your Message Cannot be Saved';
   }
   return $error;
}


















   }





?>
