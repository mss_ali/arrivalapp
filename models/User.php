<?php

 class User extends CI_Model{
   
      function __construct() {

        $this->load->database();
        $this->load->library('common');

       }
       
       /*public function getUserByEmail_Password($email,$password)
       {
             $error = array();
             $password = md5($password);  
             $array = array('email' => $email, 'password' => $password);
             $data = $this->db->get_where('users', $array)->result();
             
             if(count($data)==0)
             { 
              $error['statuscode'] = 421;
              $error['message'] = 'Invalid email or password.'; 
              
             }else{
                $resp=$this->common->UniqueKey();
                $this->update_Token($resp,$data[0]->id);
                $data[0]->token = $resp;
                $error['statuscode'] = 200;
                $error['data'] = $data[0];

             }
             return $error;

       	     
       
       }*/
       /*public function getUserByEmail_Password($email,$password,$role,$deviceId)
       {
             $error = array();
             $password = md5($password);  
             $array = array('email' => $email, 'password' => $password,'role' =>$role,'device_id'=>$deviceId);
             $data = $this->db->get_where('users', $array)->result();
             
             if(count($data)==0)
             { 
              $error['statuscode'] = 421;
              $error['message'] = 'Invalid email or password.'; 
              
             }else{
                $resp=$this->common->UniqueKey();
                $this->update_Token($resp,$data[0]->id);
                $data[0]->token = $resp;
                $error['statuscode'] = 200;
                $error['data'] = $data[0];

             }
             return $error;

             
       
       }
       public function getUserByEmail_Password($email,$password,$role,$mac_id){
         $error = array();
         $password = md5($password);  
         $array = array('email' => $email, 'password' => $password,'role' =>$role);
         $data = $this->db->get_where('users', $array)->result();
         if(count($data)==0){ 
          $error['statuscode'] = 421;
          $error['message'] = 'Invalid email or password.'; 
         }else{
            $user_mac_id=$this->updateMacId($data,$mac_id);
            $resp=$this->common->UniqueKey();
            $this->update_Token($resp,$data[0]->id);
            $data[0]->token = $resp;
            $data[0]->mac_id = $mac_id;
            $data[0]->user_mac_id = $user_mac_id;
            $error['statuscode'] = 200;
            $error['data'] = $data[0];
         }
         return $error;
       }*/






public function getUserByEmail_Password($email,$password,$role,$mac_id,$device_id){
         $error = array();
         $password = md5($password);  
         $array = array('email' => $email, 'password' => $password,'role' =>$role);
         $data = $this->db->get_where('users', $array)->result();
         // print_r($device_id);die;
         if(count($data)==0){ 
          $error['statuscode'] = 421;
          $error['message'] = 'Invalid email or password.'; 
         }else{
            $user_mac_id=$this->updateMacId($data,$mac_id);
            $resp=$this->common->UniqueKey();
           // $device_id=$this->common->UniqueKey();
            $this->update_Token($device_id,$resp,$data[0]->id);
            $data[0]->token = $resp;
            $data[0]->device_id = $device_id; 
            $data[0]->mac_id = $mac_id;
            $data[0]->user_mac_id = $user_mac_id;
            $error['statuscode'] = 200;
            $error['data'] = $data[0];
         }
         return $error;
       }

      private function updateMacId($data,$mac_id){
            $id=$data[0]->id;
            $array = array('user_id' => $id, 'mac_id' => $mac_id);
            $data = $this->db->get_where('user_macs', $array)->result();
            if(count($data)>0){
                return $data[0]->id;  
            }else{
                $sql = "INSERT INTO user_macs (user_id,mac_id) VALUES(?,?)";
                $this->db->query($sql, array($id, $mac_id));
                if($this->db->affected_rows()>0){
                  $user_mac_id=$this->db->insert_id();
                  return $user_mac_id;
                }else{
                  return false;
                } 
           }
       }


       private function updateDeviceIdAndMacId($email,$password,$role,$deviceId,$mac_id){

       
            
             
             $array = array('email' => $email, 'password' => $password,'role' =>$role);
             $data = $this->db->get_where('users', $array)->result();
             

             if($data[0]->device_id == $deviceId && $data[0]->mac_id == $mac_id){


                
                return true;  
              


              }else{
                       
                   $dataupdate = array(

                      'device_id'=>$deviceId,
                      'mac_id'=>$mac_id,
                    );

                  /*
                  Delete history code goes here..    
                  //$this->db->where('device_id',$deviceId);
                  */ 

                  $user_id = $data[0]->id;

                  $this->deleteHistoryFromLoginAnotherDevice($user_id);

                  /*Delete history end........*/ 


                   $this->db->where('id',$data[0]->id);
                   $this->db->update('users',$dataupdate);
                   if($this->db->affected_rows()>0){
                       return true;
                   } 


             }
             


             


       }
       protected function deleteHistoryFromLoginAnotherDevice($from_user_id){

        
         $array = array('from_user_id' => $from_user_id);
         $data = $this->db->get_where('messages', $array)->result();
         if(count($data)==0){
          
           return true;

         }else{


            $this->db->where('from_user_id',$from_user_id);
            $this->db->delete('messages');
            return true;

         }
             





       }


 protected function update_Token($device_id,$token,$id)
       {
                $data = array(
		               'token' => $token,
                   'device_id'=>$device_id
		              );
                //print_r($data);die;
		$this->db->where('id', $id);
	        $this->db->update('users', $data);  
       }



       /*protected function update_Token($token,$id)
       {
                $data = array(
		               'token' => $token,
		              );
		$this->db->where('id', $id);
	        $this->db->update('users', $data);  
       }*/
      public function getUserById($userid,$token){

             $error = array();
             $array = array('id' => $userid, 'token' => $token);
             $data = $this->db->get_where('users', $array)->result();
             
             if(count($data)==0)
             { 
              $error['statuscode'] = 422;
              $error['message'] = 'Unautorized user.'; 
              
             }else{
                $this->make_Token_empty($data[0]->id);
                $this->make_Device_empty($data[0]->id);
                $error['statuscode'] = 200;
                $error['message'] = 'User has been logout successfully.';

             }
             return $error;

       }
       protected function make_Token_empty($id)
       {
               $data = array(
                   'token' => '',
                  );

        $this->db->where('id', $id);
        $this->db->update('users', $data);  
       }

       protected function make_Device_empty($id)
       {
               $data = array(
                   'device_id' => '',
                  );

        $this->db->where('id', $id);
        $this->db->update('users', $data);  
       }
       /* public function getUserByEmail($email)
       {
             $error = array();
             $array = array('email' => $email);
             $data = $this->db->get_where('users', $array)->result();
             
             if(count($data)==0)
             { 
              $error['statuscode'] = 304;
              $error['message'] = 'Email does not exist.'; 
              
             }else{
                $error['statuscode'] = 200;
                $error['data'] = $data[0];

             }
             return $error;

             
       
       }*/
       public function getUserByEmail($email,$role)
       {
             $error = array();
             $array = array('email' => $email,'role'=>$role);
             $data = $this->db->get_where('users', $array)->result();
             
             if(count($data)==0)
             { 
              $error['statuscode'] = 304;
              $error['message'] = 'Email does not exist.'; 
              
             }else{
                $error['statuscode'] = 200;
                $error['data'] = $data[0];

             }
             return $error;

             
       
       }
       public function update_and_getPassword($id){
        
        $password = $this->common->get_random_password();
        
        $encrypt_password = md5($password);

        
        $this->update_password($id,$encrypt_password);
         
        return $password; 
       
       }
         /*for update password start*/
         
           public function profileupdatePasswor($id,$password){
            
                $error = $this->update_password($id,md5($password));
                if($error){
                  $response['statuscode'] = 433;
                  $response['message'] = 'Password has been updated successfully.';
                }else{
                           
                  $response['statuscode'] = 434;
                  $response['message'] = 'Something went wrong while updating the password.';        
                }
                return $response;
           }

       /*for update password end*/

       protected function update_password($id,$password)
       {
           $data = array(
                   'password' => $password,
                  );

        $this->db->where('id', $id);
         $query = $this->db->update('users', $data);  
        if($query){
          return 1;
        }else{
          return 0;
        }  
       }
       /*===========For Add Network API===========Start============*/
       public function is_autorizedUser($userid,$token){

             $error = array();
             $array = array('id' => $userid, 'token' => $token,'role'=>'user');
             $data = $this->db->get_where('users', $array)->result();
             
             if(count($data)==0)
             { 
              $error['statuscode'] = 316;
              $error['message'] = 'Unautorized user.'; 
              
             }else{
                 
                $error['statuscode'] = 200;
                

             }
             return $error;

       }
       /*===========Form Add Netwrk API===========End==============*/

  /*=================List and Search Network API Start==============*/

    public function isAuthorizedAdmin($adminId,$token){
      $error = array();
      $array = array('id' => $adminId,'token' => $token,'role'=>'admin');
      $data = $this->db->get_where('users', $array)->result();
      if(count($data)==0){ 
        $error['statuscode'] = 316;
        $error['message'] = 'Unautorized user.'; 
      }else{
          $error['statuscode'] = 200;
      }
      return $error;
    }
    
  /*=================List and Search Network API End==============*/

    /*=========For Global Feedback send to Arrival Admin Start=============*/
   public function getUser($userid){

             $error = array();
             $array = array('id' => $userid);
             $data = $this->db->get_where('users', $array)->result();
             return $data;

       }

public function getNetworks_ip($network_id){

             $error = array();
             $array = array('id' => $network_id);
              $this->db->select('networks.public_ip,networks.id');
             $data = $this->db->get_where('networks', $array)->result();
             return $data;

       }

 public function get_date_time($userid){

             $error = array();
             $array = array('id' => $userid);
              $this->db->select('messages.created_date');
             $data = $this->db->get_where('messages', $array)->result();
             return $data;

       }



        public function getUserNotify($userid){

             $error = array();
             $array = array('id' => $userid);
             $this->db->select('users.id');
             $this->db->select('users.role');
             $this->db->select('users.name');
             $this->db->select('users.email');
             $data = $this->db->get_where('users', $array)->result();
             return $data;

       }
  /*=========For Global Feedback send to Arrival Admin End===============*/

  public function checkUser($userid){
             $error = array();
             $array = array('id' => $userid,'role'=>'user');
             $data = $this->db->get_where('users', $array)->result();
             if(count($data)>0){
               $error['statuscode'] = 200;
             }else{
              $error['statuscode']=532;
              $error['msg']="User is not valid.";
             }
             return $error;
  }


  
  /*======================Get network By ID==========Start here===============*/
  public function isAuthorizedAdminByToken($token){
     $error = array();
     $array = array('token' => $token,'role'=>'admin');
     $data = $this->db->get_where('users', $array)->result();
     if(count($data)==0){ 
      $error['statuscode'] = 316;
      $error['message'] = 'Unautorized user.'; 
     }else{
        $error['statuscode'] = 200;
     }
     return $error;
  }

public function isAdminAccessOwnNetwork($token,$network_id){
    $error = array();
    $query2 = $this->db->get_where('networks', array('id' => $network_id));
    if($query2->num_rows()===0){
     $error['statuscode']=334;
     $error['message'] = 'Network does not exist.';
     return $error;
    }
    $this->db->select('users.id')->where('users.token',$token);
    $query = $this->db->get('users')->result();
    if(count($query)>0){
      $user_id = $query[0]->id;
    }
    $query1 = $this->db->get_where('networks', array('id' => $network_id,'user_id' => $user_id));
    if($query1->num_rows()===0){
     $error['statuscode']=334;
     $error['message'] = 'This network not associated with this user';
    }else{
      $error['statuscode']=200;        
    }
    return $error;
}    
  /*=======================Get network By Id=========End here==================*/


  /*

            Description

       :-Same email can be used for Admin and User

       Same email can be used for admin and user.
      : Same can not be used for more then one admin
      :- Same email can not be used for more then one user


*/

      /*----------check here only email and role--- Start-*/

  public function isUserExistEmail($email,$role=null){
           if($role){
             $condition = array(
                         
                          'email'=>$email,
                          'role'=>$role
                       );
           }else{
               $condition = array(
                          'email'=>$email
                       );
           }
             
              $this->db->where($condition);
              $query = $this->db->get('users');
              if($query->num_rows()>0){
                    return true;
                  }else{  
                    return false;
                  }


  }
/*----------check here only phone and role-- End --*/

public function isUserExistPhone($role,$code,$mobile)
  {
           
              $condition = array(
                          'country_code'=>$code,
                          'phone'=>$mobile,
                          'role'=>$role
                       );
              $this->db->where($condition);
              $query = $this->db->get('users');
              if($query->num_rows()>0){
                    return true;
                  }else{  
                    return false;
                  }


  }
/*----------check here only phone and role-- End --*/



/*---START---------Delete history---user Authorization based on token--------only----------*/
/* public function isAuthorizedUserByToken($token){

             $error = array();
             $array = array('token' => $token);
             $data = $this->db->get_where('users', $array)->result();
             
             if(count($data)==0)
             { 
              $error['statuscode'] = 316;
              $error['message'] = 'Unautorized user.'; 
              
             }else{
                 
                $error['statuscode'] = 200;
                

             }
             return $error;

       }
*/

       
/*---START---------Delete history---user Authorization based on token--------only----------*/
  public function isAuthorizedMessageByAdmin($token,$m_id){
     $error = array();
     $array = array('token' => $token);
     $data = $this->db->get_where('users', $array)->result();     
     if(count($data)==0){
      $error['statuscode'] = 316;
      $error['message'] = 'Unautorized user.';       
     }else{ 
         $admin_id = $data[0]->id;  
         $array1 = array('from_user_id' => $admin_id,'id' => $m_id);
         $data1 = $this->db->get_where('messages', $array1)->result();      
        if(count($data1)==0){ 
          $array2 = array('to_user_id' => $admin_id,'id' => $m_id);
          $data2 = $this->db->get_where('messages', $array2)->result();
          if(count($data2)==0){
            $error['statuscode'] = 316;
            $error['message'] = 'Admin not Authorize to delete this message.'; 
          }else{
            $error['statuscode'] = 200;
          }
        }else{
          $error['statuscode'] = 200;
        }     
     }
     return $error;
  }

/*-----END-------Delete history---user Authorization based on token--------only----------*/       



/*-----END-------Delete history---user Authorization based on token--------only----------*/  


/*----------------update profile----------------*/

public function updateProfile()
{
 $this->load->helper('date');
        $error = array();
        $user_id=$this->input->post('user_id');
        $mobile=$this->input->post('phone');
        $country_code=$this->input->post('country_code');
         $data = array(
              'name' => $this->input->post('name'),
              'country_code'=>$country_code,
              'phone'=> $mobile,
              'updated_date' => date('Y-m-d H:i:s')

          );
        
         $this->db->where('id', $user_id);
         $this->db->update('users', $data);
                    
         if($this->db->affected_rows()===1){
          $error['statuscode']=210;
          $error['error']='false';
          $error['message']='Profile has been updated successfully.';
           }else{

          $error['statuscode']=357;
          $error['error']='true';
          $error['message']='Error occured during update profile.';

           }
           return $error;
}




   public function isAuthorizedUserByToken($user_id,$token){

             $error = array();
             $array = array('token' => $token,'id'=>$user_id);
             $data = $this->db->get_where('users', $array)->result();
             
             if(count($data)==0)
             { 
              $error['statuscode'] = 316;
              $error['message'] = 'Unautorized user.'; 
              
             }else{
                 
                $error['statuscode'] = 200;
                

             }
             return $error;

       }

  public function isemailExist($email_id,$role){
         
       $error = array(); 

       $this->db->where('email',$email_id);
       $this->db->where('role',$role);
       $query = $this->db->get('users');
       if($query->num_rows()>0)
       {
          
          $error['statuscode']=431;
          $error['message']='Email id already exist.';
          
        }
        else{          
          $error['statuscode']=200;
        }
      return $error;
      }





/*----------check here only phone and role for update profile-- End --*/

public function isUserExistPhoneforupdate($role,$mobile,$user_id)
  {
      
              $condition = array(                         
                          'phone'=>$mobile,
                          'role'=>$role,
                          'id !='=>$user_id
                       );
              $this->db->where($condition);
              $query = $this->db->get('users');
              if($query->num_rows()>0){
                    return true;
                  }else{  
                    return false;
                  }


  }
/*----------check here only phone and role-- End --*/


/*----------------update profile code end----------*/


  /*for push notification get user check by role*/

   /*public function getUserPush($userid){

             $error = array();
             $array = array('id' => $userid,'role'=>'user');
             $data = $this->db->get_where('users', $array)->result();
              
             return $data;     

       }*/

      public function getUserPush($userid,$token){
             $error = array();
             $array = array('id' => $userid,'token'=>$token);
             $data = $this->db->get_where('users', $array)->result();
             if(count($data)>0){
              $error['statuscode']=200;
             }else{
              $error['statuscode']=316;
              $error['message']='Unautorized user.';
             } 
                
            return $error;
       }

public function reciveNotifyUserExist($user_id){
    $error = array();
    $condition = array(
             'id'=>$user_id
      );

     $this->db->where($condition);
     $data = $this->db->get('users')->result();
     if(count($data)>0){
            $error['statuscode']=200;
     }else{
         $error['statuscode']=316;
         $error['message']='Push notification recipient does not exist.';
    }
    return $error;
} 

  /*----For push notification end here----------*/  
   /*--Change from here for unique device login----START--*/ 

 /* public function isUserExistDevice($role,$deviceid)
  {
           
              $condition = array(
                         
                          'device_id'=>$deviceid,
                          'role'=>$role
                       );
              $this->db->where($condition);
              $query = $this->db->get('users');
              if($query->num_rows()>0){
                    return true;
                  }else{  
                    return false;
                  }


  }*/
  public function isUserExistDevice($role,$deviceid,$mac_id)
  {
        
       $condition = array(
                   'users.role ='=>$role,
                   'users.device_id'=>$deviceid,
                   'users.mac_id'=>$mac_id
            );    
              
     
             $this->db->where($condition);
             $query = $this->db->get('users');
             
              if($query->num_rows()>0){ 
                    return 1;
                  }else{

            
                     
                   $condition1 = array(
                   'users.role ='=>$role,
                   'users.mac_id'=>$mac_id
                     );    
              
     
                   $this->db->where($condition1);
                   $query1 = $this->db->get('users');


                    if($query1->num_rows()>0){ 
                      return 2;
                    }else{

                                  $condition2 = array(
                                     'users.role ='=>$role,
                                     'users.device_id'=>$deviceid
                                       );    
                                
                       
                                     $this->db->where($condition2);
                                     $query1 = $this->db->get('users');

                                      if($query1->num_rows()>0){
                                        return 3;
                                      }else{
                                         return false;
                                      }



                                }


                    
                  return false;


                  }
                    

            

  }


   /*--Change from here for unique device login----END--*/



   /*----check here for update profile user role ---------------*/
   public function isUserExistValidRole($role,$user_id){



           $condition = array(
                   'users.role ='=>$role,
                   'users.id'=>$user_id
                     );    
              
     
                   $this->db->where($condition);
                   $query = $this->db->get('users');
                  if($query->num_rows()>0){
                    return false;
                  }
                   return true;
       



   }
   /*----end here for user profile user role---------------------*/

   public function getDeviceId($user_id){
       $condition = array('users.id'=>$user_id);    
       $this->db->where($condition);
       $query = $this->db->get('users')->result();
       return $query;
   }

 /*----Get detail by user email code start---------------------*/

    public function isAuthorizedEmail($email){
         $error = array();
         $array = array('email' => $email, 'role' => 'user');
         $data = $this->db->get_where('users', $array)->result();
          if(count($data)>0){
              $error['statuscode']=200;
             }else{
              $error['statuscode']=316;
              $error['message']='Email does not exist.';
             } 
                
     return $error;
       }

    

 public function getUserdetailEmail($email)
        {
        $condition = array('email' => $email, 'role' =>'user');    
        $this->db->where($condition);
        $data = $this->db->get('users')->result();
        if(count($data)==0){
                $error['statuscode'] = 429;
                $error['message'] = 'No detail found.'; 
              }
              else{
                $condition = array('user_id' => $data[0]->id);    
                $this->db->where($condition)->order_by('id','desc');
                $data = $this->db->get('user_macs')->result();

                if(count($data)>0)
                {
                //$condition = array('user_id' => $data[0]->user_id,'mac_id' => $data[0]->mac_id);   
                $condition = array('user_id' => $data[0]->user_id);   
                $this->db->where($condition);
                $data = $this->db->get('devices')->result();
                  if(count($data)>0)
                  {
                    $this->db->select('users.*')
                    ->select('user_macs.id AS user_mac_id')
                    ->select('user_macs.mac_id AS mac_id')
                    ->select('devices.device_name')
		     ->select('devices.id AS device_id')
                    ->from('users')
                     ->join('user_macs','user_macs.user_id=users.id')
                     ->join('devices','devices.user_id=user_macs.user_id')
                     ->limit(1)
                     ->order_by('user_macs.id','desc')
                     ->where(array('users.email' => $email, 'users.role' =>'user'));
                      $data=$this->db->get()->result(); 
                      $error=$data;
                  }
                  else
                  {
                  $this->db->select('users.*')
                  ->select('user_macs.id AS user_mac_id')
                 ->select('user_macs.mac_id AS mac_id')                  
                  ->from('users')
                   ->join('user_macs','user_macs.user_id=users.id')
                   ->limit(1)
                   ->order_by('user_macs.id','desc')
                   ->where(array('users.email' => $email, 'users.role' =>'user'));
                    $data=$this->db->get()->result_array();
		    $data[0]['device_id'] ='';
                    $data[0]['device_name'] ='';
                    $error=$data;

                  }
                  

                }
               
               
            }
            return $error;

        }


/*----Get detail by user email code end---------------------*/

/*-----------token authorized--------------*/

public function isAuthorizedUserToken($token){
     $error = array();
     $array = array('token' => $token,'role'=>'user');
     $data = $this->db->get_where('users', $array)->result();
     if(count($data)==0){ 
      $error['statuscode'] = 316;
      $error['message'] = 'Unautorized user.'; 
     }else{
        $error['statuscode'] = 200;
     }
     return $error;
  }

  /*-----------token authorized code end--------------*/
 }
?>
