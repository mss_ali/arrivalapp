<?php 
class Network extends CI_Model
{
	function __construct()
	{
		$this->load->database();
	}
	/*public function getNetworks($user_id,$token)
	{
	$error = array();	
   	$array = array('id' => $user_id,'token'=>$token);
    $user_data = $this->db->get_where('users', $array)->result(); 
    
    if(!is_numeric($user_id))
      {
        $error['statuscode']=313;
        $error['message']='User id should be numeric.';
      }   
    	else if(count($user_data)==0)
   	{
   		$error['statuscode'] = 316;
        $error['message'] = 'Unauthorized user.'; 
   	}
    
   	else{
   		$this->db->select("*");
   		$this->db->from("networks");
    	$this->db->where('user_id', $user_id);
   		$network_data= $this->db->get()->result();
   	    $error['statuscode'] = 221;
        $error['network_list'] =$network_data;
        }
        return $error;
   	
	}*/

public function getNetworks($user_id,$token){
    $error = array(); 
    $array = array('id' => $user_id,'token'=>$token,'role'=>'user');
    $user_data = $this->db->get_where('users', $array)->result(); 
    if(!is_numeric($user_id)){
      $error['statuscode']=313;
      $error['message']='User id should be numeric.';
    }else if(count($user_data)==0){
      $error['statuscode'] = 316;
      $error['message'] = 'Unauthorized user.'; 
    }else{
      $this->db->select('networks.*')->from('networks')->join('network_device','networks.id=network_device.network_id')->join('devices','network_device.device_id=devices.id')->group_by('networks.id')->where(array('devices.user_id'=>$user_id));
      $network_data = $this->db->get()->result();
      if(!empty($network_data)){
         $error['statuscode'] = 221;
         $error['network_list'] =$network_data;
      }else{
         $error['statuscode'] = 438;
         $error['network_list'] ='No network found.';
      }
    }
    return $error;
}

   
   public function getMac_id($user_id){

 
           $this->db->where('id',$user_id);
           $query = $this->db->get('users')->result();
           
 
       
           if(count($query)>0){
              return $query[0]->mac_id;
           }else{

               return false;
             }
 }


 public function getAdminIdFromDevice($mac_id){

            $this->db->where('mac_id',$mac_id);
            $query = $this->db->get('devices')->result();

           if(count($query)>0){
              return $query[0]->id;
           }else{

               return false;
             }


            

 }















  public function insertNetwork(){
        $this->load->helper('date');
        $error = array();
         $data = array(
              'user_id' => $this->input->post('user_id'),
              'name' => $this->input->post('name'),
              'ssid' => $this->input->post('ssid'),
              'network_type' => $this->input->post('network_type'),
              'router_type' => $this->input->post('router_type'),
              //'public_ip' => $this->input->post('publicip'),
              'network_key' => $this->input->post('netw_key'),
              'router_id' => $this->input->post('router_id'),
              'created_date' => date('Y-m-d H:i:s'),
              'updated_date' => date('Y-m-d H:i:s')

          );
         $this->db->insert('networks', $data); 
            
         if($this->db->affected_rows()===1){
          $error['statuscode']=210;
          $error['error']='false';
          $error['message']='Network has been added successfully.';
           }else{

            $error['statuscode']=357;
          $error['error']='true';
          $error['message']='Error occured during add network.';

           }
           return $error;

       }
       /*public function isSSISexist($user_id,$ssid){

            
           $error = array();

           $query = $this->db->get_where('networks', array('ssid' => $ssid,'user_id'=>$user_id));

           if($this->db->affected_rows()===1){
          $error['statuscode']=356;
          $error['error']='true';
          $error['message']='Network already exist please enter another SSID.';
           }else{
            $error['statuscode']=200;
          }

           return $error;


       }
       public function isSSISexist($ssid){

            
           $error = array();

           $query = $this->db->get_where('networks', array('ssid' => $ssid));

           if($this->db->affected_rows()===1){
          $error['statuscode']=356;
          $error['error']='true';
          $error['message']='Network already exist please enter another SSID.';
           }else{
            $error['statuscode']=200;
          }

           return $error;


       }*/

public function isSSISexist($ssid){

           $error = array();
           $query = $this->db->get_where('networks', array('ssid' => $ssid));
           if($this->db->affected_rows()>0){
          $error['statuscode']=356;
          $error['error']='true';
          $error['message']='Network Already Exist, Please Enter Another SSID';
           }else{
            $error['statuscode']=200;
          }
           return $error;


       }


         public function isNameexist($name){
           $error = array();
           $query = $this->db->get_where('networks', array('name' => $name));
           if($this->db->affected_rows()>0){
          $error['statuscode']=900;
          $error['error']='true';
          $error['message']='Network Already Exist, Please Enter Another Title';
           }else{
            $error['statuscode']=200;
          }

           return $error;


       }
       public function isRouterExist($router_id){

 
           $error = array();

           $query = $this->db->get_where('routers', array('id'=>$router_id));

         if($this->db->affected_rows()===1){
          $error['statuscode']=200;
           }else{
            
            $error['statuscode']=358;
            $error['error']='true';
            $error['message']='Invalid Router ID.';
          }

           return $error;
           

             
      }
/*------------listAndSearchNetwork------Start*/
public function list_SearchNetwork($data){
  $networks = array();
  $search=@$data['search'];
  $admin_id=$data['admin_id'];
  if($search){
        $this->db->select('*')->from('networks')->where(array('networks.user_id'=>$admin_id,'networks.name LIKE'=>"%$search%"));
  }else{
        $this->db->select('*')->from('networks')->where(array('networks.user_id'=>$admin_id));
  }
  $networks = $this->db->get();
  return $networks->result();
} 
/*------------listAndSearchNetwork------END*/


        /*----Get network Detail ----Start here---and --isNetworkExist----------*/
      public function get($network_id = NULL){
            
           if(!isset($network_id) && empty($network_id))
           {  
           $network_id = $this->input->post('netw_id');
           $this->db->where('id',$network_id);
           $query = $this->db->get('networks');
           return $query->result();
           }else{
           $this->db->where('id',$network_id);
           $query = $this->db->get('networks');
           return $query->result();

           } 


      }
      /*----Get network Detail-----End here----------------*/

       /*----------Delete Network Start here ------Date 15 jan 2016---------------*/

   /*
          * will check the network id in network model previosly
          * first delete from network table 
          * and then find all device_id from network_device model associated with this network_id
          * and delete from device table all id come from network_device table as device_id
   */


public function delete(){
    $error = array();
    $flag = false;
    $network_id = $this->input->post('netw_id');
    $flag = $this->deleteFromNetworkDevice($network_id);
    if($flag){
            $flag = $this->deleteFromNetworks($network_id);
            if($flag){
               $error['statuscode'] = 214;
               $error['message'] = 'Network has been deleted successfully.'; 
             }else{
                $error['statuscode'] = 441;
                $error['message'] = 'Something went wrong while delete network.';
             }  
    }else{
      $error['statuscode'] = 441;
      $error['message'] = 'Something went wrong while delete network.';
    }
    return $error; 
}

private function deleteFromNetworkDevice($network_id){
    $this->db->where('network_id',$network_id);
    if($this->db->delete('network_device')){
      return true;
    }else{
      return false;
    }
}

    

private function deleteFromNetworks($network_id){
  $this->db->where('id',$network_id);
  if(
     $this->db->delete('networks')
    ){
  return true;
  }else{
  return false;
  }
}





    /*-----------Delete Network End here--------date 15 Jan 2016----------------*/  


    

 /*---------Start From here for Search And List the network-----------------*/
   
   /*public function searchNetworkListNetwork(){
        $user_id = $this->input->post('user_id');
        $search_key = $this->input->post('search_key');
       
       if(isset($search_key) && !empty($search_key)){

        $this->db->select('*',
                          'network_device.*'
                           )
                  ->from('networks')
                  ->join('network_device','network_device.network_id=networks.id')
                  ->like('networks.name', $search_key);          
                    
                    $query = $this->db->get();   
               
                     return $query->result();

                 }else{
                          
     
                       $this->db->select('*',
                          'networks.*',
                          'network_device.*'
                           )
                  ->from('messages')
                  ->where('messages.to_user_id',$user_id)
                  ->join('networks','networks.id=messages.network_id')
                  ->join('network_device','network_device.network_id=messages.network_id');
                   $query = $this->db->get();   
               
                     return $query->result();
                } 
   } */

   public function searchNetworkListNetwork(){ 
        $user_id = $this->input->post('user_id');
        $search_key = $this->input->post('search_key');
        if(isset($search_key) && !empty($search_key)){
         $this->db->select('users.phone')
                  ->select('networks.*')
                  ->from('networks')
                  ->where("network_type !=",'private')
                  ->like('networks.name', $search_key)
                  ->join('users','users.id=networks.user_id');
                    $query = $this->db->get();   
                    return $query->result();
                 }else{
                    $mac_id = $this->getMac_id($user_id);
                    $networkOwnerId = $this->getAdminIdFromDevice($mac_id);
                    $this->db->select('network_device.device_id');
                    $this->db->select('networks.*');
                    $this->db->join('networks','network_device.network_id=networks.id');
                    $this->db->from("network_device");
                    $this->db->where('device_id', $networkOwnerId);
                    $query = $this->db->get();   
                    return $query->result();
                } 
   } 
 /*---------End here for search and list the network-------------------------*/




 

 /*----------------------------Get Recent Network Ten by user----------Start------------------*/

 
   public function getRecentTenConnectedInfo(){
       $user_id = $this->input->post('user_id');
       /*$this->db->select('*','networks.*','network_device.*')
               ->from('messages')
               ->where('messages.to_user_id',$user_id)
               ->join('networks','networks.id=messages.network_id')
               ->join('network_device','network_device.network_id=messages.network_id')
               ->limit(10)
               ->order_by('messages.id','desc');*/

        $this->db->select('networks.*')->select('devices.created_date AS connected_date')->from('networks')->join('network_device','networks.id=network_device.network_id')->join('devices','network_device.device_id=devices.id')->where(array('devices.user_id'=>$user_id))->limit(10)
               ->order_by('devices.created_date','desc');

        $query = $this->db->get();
        return $query->result();
   }


 /*-----------------------------Get Recent Network Ten by user----------End ----Here----------*/


      /*--------------list device---------------*/
 public function DevicesperNetwork(){
   $netw_id = $this->input->post('netw_id');
   $data=array();
   $this->db->select('networks.id AS network_id')->select('devices.*')->where('network_device.network_id',$netw_id)->join('networks','networks.id=network_device.network_id')->join('devices','network_device.device_id=devices.id');
   $result_data=$this->db->get('network_device')->result();
   if(count($result_data)==0){
          $error['statuscode']=200;
          $error['error']='true';
          $error['message']='No device found with this network';
   }else{
          $error['statuscode']=200;
          $error['error']='false';
          $error['network_count']=count($result_data);
          $error['network_list']=$result_data;
   } 
   return $error;
 }
/*------------------list device code end-------------*/


 /*-------FOR UPDATE NETWORK ----------------*/

     public function updateNetwork(){

          
        $this->load->helper('date');
        $error = array();
         $data = array(
              'name' => $this->input->post('name'),
              'ssid' => $this->input->post('ssid'),
              'network_type' => $this->input->post('network_type'),
              'router_type' => $this->input->post('router_type'),
              'public_ip' => $this->input->post('publicip'),
              'network_key' => $this->input->post('netw_key'),
              'router_id' => $this->input->post('router_id'),
              'updated_date' => date('Y-m-d H:i:s'),
          );

         $this->db->where('id', $this->input->post('network_id'));
         $this->db->update('networks', $data);
            
         if($this->db->affected_rows()===1){
          $error['statuscode']=436;
          $error['error']='false';
          $error['message']='Network has been updated successfully.';
           }else{

            $error['statuscode']=357;
          $error['error']='true';
          $error['message']='Error occured during update network.';

           }
           return $error;






     }
     /*For edit network if ssid exist or not apart from user(Admin) ID*/

   /* public function isSSISexistEdit($user_id,$ssid){


       $error = array();


           $query = $this->db->get_where('networks', array('ssid' => $ssid,'user_id !='=>$user_id));

           if($this->db->affected_rows()===1){
          $error['statuscode']=356;
          $error['error']='true';
          $error['message']='Network already exist please enter another SSID.';
           }else{
            $error['statuscode']=200;
          }

           return $error;


    }
*/
    public function isSSISexistEdit($network_id,$ssid){


       $error = array();


           $query = $this->db->get_where('networks', array('ssid' => $ssid,'id !='=>$network_id));

           if($this->db->affected_rows()===1){
          $error['statuscode']=356;
          $error['error']='true';
          $error['message']='Network already exists with this SSID,Please enter another SSID.';
           }else{
            $error['statuscode']=200;
          }

           return $error;


    }

   /*End*/
 





   /*---END UPDATE NETWORK HERE----------------*/


/*--------Get all network with get_request code start--------*/

public function getallNetworks(){
    $error = array(); 

       $search_key = $this->input->post('search_key');       
       if(isset($search_key) && !empty($search_key)){ 
        $this->db->select('*')->from('networks')->group_by('networks.id')->like('networks.name', $search_key)->where(array('networks.network_type !='=>'Private'));
        $network_data = $this->db->get()->result();
       }
       else{
        $this->db->select('*')->from('networks')->group_by('networks.id')->where(array('networks.network_type !='=>'Private'));
        $network_data = $this->db->get()->result();
       } 
       if(!empty($network_data)){
         $error['count_list'] =count($network_data);
         $error=$network_data;
        }else{
         $error['statuscode'] = 438;
         $error['network_list'] ='No network found.';
      }
    
    return $error;
}

/*--------Get all network with get_request code end--------*/ 

/*---------------delete user network -----------------*/

        public function deleteuserNetwork(){
            $error = array();
            $flag = false;
            $network_id = $this->input->post('netw_id');
            $token = $this->input->post('token');
            $user_id=$this->FromtokenUserid($token);
           
            $net_delete = $this->deleteFromuserNetworkDevice($network_id,$user_id[0]->id);
                   if($net_delete){
                       $error['statuscode'] = 214;
                       $error['message'] = 'Network has been deleted successfully.'; 
                     }else{
                        $error['statuscode'] = 441;
                        $error['message'] = 'Something went wrong while delete network.';
                     }  
           
            return $error; 
        }

        private function deleteFromuserNetworkDevice($network_id,$user_id){
          $data = array();
          $this->db->where('user_id',$user_id);
          $data = $this->db->get('devices')->result_array();
          $device_id = array();
          foreach ($data as $key => $value) {
                       array_push($device_id, $value['id']);
                      }
         if(count($device_id)>0)
          {          
           $this->db->where_in('device_id',$device_id);  
           $this->db->where('network_id',$network_id);
            if($this->db->delete('network_device')){
              foreach ($device_id as $key => $value) {
                 $this->db->select('*')->from('network_device')->where(array('network_device.device_id'=>$value));
                 $isConnected = $this->db->get()->result();
                 if(count($isConnected)<1){
                    $this->db->where('id',$value);
                    $this->db->delete('devices');
                 }
               }
              return true;
            }else{
              return false;
            }

          }
        }
         
         public function FromtokenUserid($token)
        {
          $this->db->select('id')
          ->from('users')
          ->where('token',$token);
          $user_data = $this->db->get()->result();
          return $user_data;
           }







/*-------------------code end----------------------*/


 


}

?>
