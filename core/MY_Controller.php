<?php
class MY_Controller extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
       
    }
    function send_Email($from,$to,$subject=null,$message=null){
      
   
      

     $error = array();

      //$this->load->library('email');

      $config = Array(       
            'protocol' => 'sendmail',
            //'smtp_host' => 'your domain SMTP host',
            //'smtp_port' => 25,
            //'smtp_user' => 'SMTP Username',
            //'smtp_pass' => 'SMTP Password',
            //'smtp_timeout' => '4',
            'mailtype'  => 'html',
            'charset'   => 'iso-8859-1'
        );
        $this->load->library('email', $config);

      $this->email->set_newline("\r\n");
      
      $this->email->from($from, 'Arrival App');
      
      $this->email->to($to);
      
      $this->email->subject($subject);
     
      $this->email->message($message);

      if(!$this->email->send()){
         
         $error['statuscode'] = 505;
      }
      else{

          $error['statuscode'] = 200;
      }
      return $error;

     
      
    
    }
} 
