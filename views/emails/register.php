<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Arrival WIFI</title>
      
      <style type="text/css">
	    @import url(http://fonts.googleapis.com/css?family=Merienda);
         /* Client-specific Styles */
         #outlook a {padding:0;} /* Force Outlook to provide a "view in browser" menu link. */
         body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
         /* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
         .ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */
         .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing.  More on that: http://www.emailonacid.com/forum/viewthread/43/ */
         #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
         img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}
         a img {border:none;}
         .image_fix {display:block;}
         p {margin: 0px 0px !important;}
         table td {border-collapse: collapse;}
         table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
         a {color: #33b9ff;text-decoration: none;text-decoration:none!important;}
         /*STYLES*/
         table[class=full] { width: 100%; clear: both; }
		 
         /*IPAD STYLES*/
         @media only screen and (max-width: 640px) {
         a[href^="tel"], a[href^="sms"] {
         text-decoration: none;
         color: #33b9ff; /* or whatever your want */
         pointer-events: none;
         cursor: default;
         }
         .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
         text-decoration: default;
         color: #33b9ff !important;
         pointer-events: auto;
         cursor: default;
         }
         table[class=devicewidth] {width: 440px!important;text-align:center!important;}
         table[class=devicewidthinner] {width: 420px!important;text-align:center!important;}
         table[class=mainsmall1] { float:left!important;}
         table[class=mainsmall2] { float:right!important;}
         table[class=banner-gap] { display:none!important;}
         img[class="bannerbig"] {width: 440px!important;height:371px!important;}
         img[class="spacinglines"]{width: 420px!important;}
         
         
         }
         /*IPHONE STYLES*/
         @media only screen and (max-width: 480px) {
         a[href^="tel"], a[href^="sms"] {
         text-decoration: none;
         color: #33b9ff; /* or whatever your want */
         pointer-events: none;
         cursor: default;
         }
         .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
         text-decoration: default;
         color: #33b9ff !important; 
         pointer-events: auto;
         cursor: default;
         }
         table[class=devicewidth] {width: 280px!important;text-align:center!important;}
         table[class=devicewidthinner] {width: 260px!important;text-align:center!important;}
         table[class=mainsmall1] { float:left!important;width: 120px!important;height:90px!important;}
         table[class=mainsmall2] { float:right!important;width: 120px!important;height:90px!important;}
         img[class=mainsmall1] { width: 120px!important;height:90px!important;}
         img[class=mainsmall2] { width: 120px!important;height:90px!important;}
         table[class=banner-gap] { display:none!important;}
         img[class="bannerbig"] {width: 280px!important;height:236px!important;}
         img[class="spacinglines"]{width: 260px!important;}
         img[class="view_mr"] {height: inherit !important; width: 100% !important; }
        
         }
      </style>
   </head>
   <body style="font-family: Helvetica, arial, sans-serif;">
 <table width="100%" bgcolor="#eee" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
   <tbody>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
         <td>
           <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" >
               <tbody>
                  <tr>
                     <td width="100%"></td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- Start of header -->
<table width="100%" bgcolor="#eee" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
   <tbody>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
         <td>
           <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" >
               <tbody>
                  <tr>
                     <td width="100%">
                        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" bgcolor="#fff">
                           <tbody>
                              <!-- Spacing -->
                              <tr>
                                 <td width="100%" height="10"></td>
                              </tr>
                              <!-- Spacing -->
                              <tr>
                                 <td style="background:#fff">
                                    <!-- logo -->
                                    <table width="100%" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth" >
                                       <tbody>
                                          <tr>
                                            <td width="30" align="left">&nbsp;</td>
                                             <td width="100%" height="50" align="center">
                                                <div class="imgpop">
                                                   <a target="_blank" href="#">
                                                   <img src="http://mastersoftwaretechnologies.com/design_review/arrival-app/arrival-logo.png" alt="" border="0" width="140" style="display:block; border:none; outline:none; text-decoration:none;">
                                                   </a>
                                                </div>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <!-- end of logo -->
                                 </td>
                              </tr>
                              <!-- Spacing -->
                              <tr>
                                 <td width="100%" height="10"></td>
                              </tr>
                              <!-- Spacing -->
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- End of Header -->
<!-- Start of menu --><!-- End of menu -->     

<table width="100%" bgcolor="#eee" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="full-text">
   <tbody>
      <tr>
         <td>
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="background:url(http://mastersoftwaretechnologies.com/design_review/arrival-app/arrival-thanks-bg.png) no-repeat">
               <tbody>
                <tr>
                     <td width="100%" height="5"></td>
                  </tr>
                  <tr>
                     <td width="100%">
                        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                           <tbody>
                              <!-- Spacing -->
                           
                              <!-- Spacing -->
                              <tr>
                                 <td>
                                    <table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
                                       <tbody>
                                        
                                          <!-- Title -->
                                        
                                       
                                          <tr>
                                             <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                          </tr>
                                       
                                          <tr>
                                            <td style="font-family: Helvetica, arial, sans-serif; font-size: 34px; text-transform:uppercase; color: #fff; text-align:center; line-height:34px;">&nbsp;</td>
                                          </tr>
                                          <tr>
                                            <td style="font-family: Helvetica, arial, sans-serif; font-size: 28px; text-transform:uppercase; color: #fff; text-align:center; line-height:34px;">
                                                Welcome <b>Arrival Wifi</b>
                                            </td>
                                          </tr>
                                            <tr>
                                             <td width="100%" height="5" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                          </tr>
                                          
                                          <!-- End of Title -->
                                          <!-- spacing -->
                                          <tr>
                                             <td width="100%" height="5" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                          </tr>
                                          <!-- End of spacing -->
                                          <!-- content -->
                                          <tr>
                                            <td style="font-family: Helvetica, arial, sans-serif; text-transform:uppercase; font-size: 16px; color: #fff; text-align:center; line-height: 24px; padding-bottom: 10px;">
                                            Thanks for signing up for Arrival Wifi! We're excited<br />
to have you onboard. </td>
                                          </tr>
                                          <!-- End of content -->
                                        
                                    <!-- content -->
                       		           <tr>
                       		        
                                             <tr>
                                               <td>&nbsp;</td>
                                             </tr>
                                             <tr>
                                             <td>&nbsp;
                                             </td>
                                          </tr>
                                          
                                          <tr>
                                              <td width="100%" height="5"></td>
                                          </tr>
                                          <!-- End of content -->
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                              <!-- Spacing -->                              <!-- Spacing -->
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
  

<!-- text -->
<table width="100%" bgcolor="#eee" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="full-text">
   <tbody>
      <tr>
         <td>
            <table width="600" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" >
               <tbody>
                  <tr>
                     <td width="100%">
                        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                           <tbody>
                              <!-- Spacing -->
                           
                              <!-- Spacing -->
                              <tr>
                                 <td>
                                    <table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
                                       <tbody>
                                        
                                          <!-- Title -->                                          <!-- End of Title -->
                                          <!-- spacing -->                                          <!-- End of spacing -->
                                          <!-- content -->
                                          <tr>
                                            <td style="font-family: Helvetica, arial, sans-serif; font-size: 19px; color: #f03861; text-align:center; line-height: 24px;">&nbsp;</td>
                                          </tr>
                                          <tr>
                                            <td style="font-family: Helvetica, arial, sans-serif; font-size: 19px; color: #f03861; text-align:center; line-height: 24px;">&nbsp;</td>
                                          </tr>
                                          <tr>
                                            <td style="font-family: Helvetica, arial, sans-serif; font-size: 16px; color: #353535; text-align:center; line-height: 24px;">Your Email Id: <strong><?php echo $data['email'] ?></strong>, and your password is<strong> <?php echo $data['confirm_password'] ?></strong></td>
                                          </tr>
                                         
                                          <tr>
                                            <td style="font-family: Helvetica, arial, sans-serif; font-size: 16px; color: #353535; text-align:center; line-height: 24px;">&nbsp;</td>
                                          </tr
                                          
                                          >
                                          
                                          <!-- <tr>
                                            <td style="padding-top: 23px; text-align: center;">
                                               <a style="color: #38af00;" href="#">http://www.arrivalwifi.com/214_4n6aqkj1jdic08kko0gs</a>
                                            </td>
                                          </tr> -->



                                          <!-- End of content -->
                                          <!-- spacing -->
                                          <tr>
                                             <td width="100%" height="10" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                          </tr>
                                    <!-- content -->
                                             <tr>
                                                  <td style="font-family: Helvetica, arial, sans-serif; font-size:15px; text-transform:uppercase; color: #000; text-align:center; line-height: 24px;">&nbsp;</td>
                                         </tr>
                                            
                                          <!-- End of content -->
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                              <!-- Spacing -->
                            
                              <!-- Spacing -->
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- end of text -->
<!-- 3 columns --><!-- end of 3 columns -->
<!-- start of call to action -->
<table width="100%" bgcolor="#eee" cellpadding="0" cellspacing="0" border="0" id="backgroundTable">
   <tbody>
      <tr>
         <td>
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
               <tbody>
                  <tr>
                     <td width="100%">
                        <table bgcolor="#38af00" width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                           <tbody>
                              <!-- Spacing -->
                              <tr>
                                 <td height="20"></td>
                              </tr>
                              <!-- Spacing -->
                              <tr>
                                 <td>
                                    <table width="560" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                       <tbody>
                                          <tr>
                                             <td>
                                                <!-- start left column--><!-- end of left column -->
                                                <!-- spacing for mobile -->
                                                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                                   <tbody>
                                                    
                                                      <tr>
                                                        <td  align="center" ><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                          <tr>
                                                            <td width="29%">&nbsp;</td>
                                                            <td width="42%" align="center"><table width="60%" border="0" cellspacing="0" cellpadding="0">
                                                              <tr>
                                                                <td align="center"><a target="_blank" href="#"><img src="http://mastersoftwaretechnologies.com/design_review/arrival-app/fb.png" alt="fb"/></a></td>
                                                                <td align="center"><a target="_blank" href="#"><img src="http://mastersoftwaretechnologies.com/design_review/arrival-app/twt.png" alt="twitter"/></a></td>
                                                                <td align="center"><a target="_blank" href="#"><img src="http://mastersoftwaretechnologies.com/design_review/arrival-app/linkedin.png" alt="linkedin"/></a></td>
                                                              </tr>
                                                            </table></td>
                                                            <td width="29%">&nbsp;</td>
                                                          </tr>
                                                        </table></td>
                                                      </tr>
                                                      <tr>
                                                         <td  align="center" style="font-family: Helvetica, arial, sans-serif; font-size:15px; padding-top: 10px;">
                                                         <p style="color:#fff; text-align:center; font-size:14px; line-height: 22px;">You are receiving this message because you have signed up at <a href="#" target="_blank" style="text-decoration:underline; color:#fff">arrivalwifi.com</a> to receive our email communications.
                                                         </p></td>
                                                      </tr>
                                                     
                                                   </tbody>
                                                </table>
                                                <!-- end spacing for mobile -->
                                                <!-- start of right column --><!-- end of right column -->
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                              <!-- Spacing -->
                              <tr>
                                 <td height="20"></td>
                              </tr>
                              <!-- Spacing -->
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- end of call to action -->
<!-- Start of preheader -->
<table width="100%" bgcolor="#eee" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="preheader" >
   <tbody>
      <tr>
         <td>
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
               <tbody>
                  <tr>
                     <td width="100%">
                        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                           <tbody>
                              <!-- Spacing -->
                              <tr>
                                 <td width="100%" height="20"></td>
                              </tr>
                              <!-- Spacing -->
                              <tr>
                                 <td>
                                    <table width="600" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                       <tbody>
                                          <tr>
                                             <td width="403" align="center" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 12px;color: #666666" st-content="viewonline">
                                                If you do not want to hear from us please <a href="#" style="text-decoration:none;color:#38af00">unsubscribe</a>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                              <!-- Spacing -->
                              <tr>
                                 <td width="100%" height="20"></td>
                              </tr>
                              <!-- Spacing -->
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- End of preheader -->    

   </body>
   </html>